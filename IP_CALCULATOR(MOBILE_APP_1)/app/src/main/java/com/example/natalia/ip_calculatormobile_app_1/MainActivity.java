package com.example.natalia.ip_calculatormobile_app_1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;


public class MainActivity extends AppCompatActivity {

    RadioGroup rgMask;
    RadioButton radioDefault;
    RadioButton radioCustomize;

    TextView txtIPOctet1;
    TextView txtIPOctet2;
    TextView txtIPOctet3;
    TextView txtIPOctet4;

    TextView txtMaskOctet1;
    TextView txtMaskOctet2;
    TextView txtMaskOctet3;
    TextView txtMaskOctet4;

    TextView lblNumOfSubNet;
    TextView txtNumOfSubNet;
    TextView lblHostsPerNetwork;
    TextView txtHostsPerNetwork;

    Button btnCalculate;

    ImageView imgHelp;
    ImageView imgAbout;

    ListView listNetworks;
    ArrayAdapter adapter;

    static int[] ip;
    static int[] mask;
    static int maskLength;
    static int defaultMaskLength;
    static int[] defaultMask = new int[] {0, 0, 0, 0};
    static char ipClass;
    static int numOfHosts;
    static int numOfNetworks;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeComponents();
    }



    public boolean checkOctets(TextView... textViews) {
        for (TextView textView: textViews) {
            if (!checkOctetText(textView)) return false;
        }
        return  true;
    }

    public boolean checkOctetText(TextView textView) {
        if (!textView.getText().toString().isEmpty()) {
            int num = Integer.parseInt(textView.getText().toString());
            if (num >= 0 && num < 256) return true;
        }
        textView.setError("0-255");
        return false;
    }



    public boolean checkSubnetMask(TextView... textViews) {
        int prevNum = 255;
        for (TextView textView: textViews) {
            String sMask = textView.getText().toString();
            if (sMask.isEmpty()) return false;              //field is empty -> error
            else {                                          //is not empty ->
                int numMask = Integer.parseInt(sMask);      // parse to integer
                System.out.println(numMask+" "+prevNum);
                if (numMask > prevNum) {                   // compare with the previous octet of mask
                    //throw error                           // if it's more -> error
                    textView.setError("Invalid mask");
                    return false;
                } else {                                      //if it's less or equal -> check the number

                    if (numMask != 0 && prevNum != 255) {
                        textView.setError("Invalid mask");
                    } else {

                        switch (numMask) {
                            case 255:
                                maskLength += 8;
                                prevNum = numMask;
                                textView.setError(null);//go further , change prevNum for comparing
                                break;
                            case 254:
                                maskLength += 7;
                                prevNum = numMask;
                                textView.setError(null);//go further , change prevNum for comparing
                                break;
                            case 252:
                                maskLength += 6;
                                prevNum = numMask;
                                textView.setError(null);//go further , change prevNum for comparing
                                break;
                            case 248:
                                maskLength += 5;
                                prevNum = numMask;
                                textView.setError(null);//go further , change prevNum for comparing
                                break;
                            case 240:
                                maskLength += 4;
                                prevNum = numMask;
                                textView.setError(null);//go further , change prevNum for comparing
                                break;
                            case 224:
                                maskLength += 3;
                                prevNum = numMask;
                                textView.setError(null);//go further , change prevNum for comparing
                                break;
                            case 192:
                                maskLength += 2;
                                prevNum = numMask;
                                textView.setError(null);//go further , change prevNum for comparing
                                break;
                            case 128:
                                maskLength += 1;
                                prevNum = numMask;
                                textView.setError(null);//go further , change prevNum for comparing
                                break;
                            case 0:
                                prevNum = numMask;
                                textView.setError(null);//go further , change prevNum for comparing
                                break;
                            default:
                                return false;
                        }
                    }
                }
            }
        }


        return true;
    }


    public void initializeComponents() {

        ip = new int[4];
        mask = new int[4];

        txtIPOctet1 = (TextView) findViewById(R.id.txtIPOctet1);
        txtIPOctet1.requestFocus();
        txtIPOctet2 = (TextView) findViewById(R.id.txtIPOctet2);
        txtIPOctet3 = (TextView) findViewById(R.id.txtIPOctet3);
        txtIPOctet4 = (TextView) findViewById(R.id.txtIPOctet4);
        txtMaskOctet1 = (TextView) findViewById(R.id.txtMaskOctet1);
        txtMaskOctet2 = (TextView) findViewById(R.id.txtMaskOctet2);
        txtMaskOctet3 = (TextView) findViewById(R.id.txtMaskOctet3);
        txtMaskOctet4 = (TextView) findViewById(R.id.txtMaskOctet4);

        lblNumOfSubNet = (TextView) findViewById(R.id.lblNumOfSubNet);
        txtNumOfSubNet = (TextView) findViewById(R.id.txtNumOfSubNet);
        lblHostsPerNetwork = (TextView) findViewById(R.id.lblHostsPerNetwork);
        txtHostsPerNetwork = (TextView) findViewById(R.id.txtHostsPerNetwork);

        rgMask = (RadioGroup) findViewById(R.id.rgHosts);
        radioDefault = (RadioButton) findViewById(R.id.radioDefault);

        radioDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lblNumOfSubNet.setVisibility(View.INVISIBLE);
                txtNumOfSubNet.setVisibility(View.INVISIBLE);
                lblHostsPerNetwork.setVisibility(View.INVISIBLE);
                txtHostsPerNetwork.setVisibility(View.INVISIBLE);
            }
        });
        radioCustomize = (RadioButton) findViewById(R.id.radioCustomize);
        radioCustomize.toggle();
        radioCustomize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lblNumOfSubNet.setVisibility(View.VISIBLE);
                txtNumOfSubNet.setVisibility(View.VISIBLE);
                lblHostsPerNetwork.setVisibility(View.VISIBLE);
                txtHostsPerNetwork.setVisibility(View.VISIBLE);
            }
        });

        listNetworks = (ListView) findViewById(R.id.listNetworks);


        btnCalculate = (Button) findViewById(R.id.btnCalculate);

        imgAbout = (ImageView) findViewById(R.id.imgAbout);
        imgAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), AboutActivity.class);
                startActivity(intent);
            }
        });

        imgHelp = (ImageView) findViewById(R.id.imgHelp);
        imgHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), HelpActivity.class);
                startActivity(intent);
            }
        });

        btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                maskLength = 0;
                if (checkOctets(txtIPOctet2, txtIPOctet3, txtIPOctet4) &&      //check IP address
                        checkSubnetMask(txtMaskOctet1, txtMaskOctet2, txtMaskOctet3, txtMaskOctet4)) { //check Mask, order is important!
                    ip[0] = Integer.parseInt(txtIPOctet1.getText().toString());
                    if (ip[0] == 0 || ip[0] == 255 || ip[0] == 127) {
                        if (ip[0] == 0) txtIPOctet1.setError("wildcard");
                        if (ip[0] == 255) txtIPOctet1.setError("broadcast");
                        if (ip[0] == 127) txtIPOctet1.setError("localhost");
                    } else {

                        txtIPOctet1.setError(null);
                        ip[1] = Integer.parseInt(txtIPOctet2.getText().toString());
                        ip[2] = Integer.parseInt(txtIPOctet3.getText().toString());
                        ip[3] = Integer.parseInt(txtIPOctet4.getText().toString());

                        mask[0] = Integer.parseInt(txtMaskOctet1.getText().toString());
                        mask[1] = Integer.parseInt(txtMaskOctet2.getText().toString());
                        mask[2] = Integer.parseInt(txtMaskOctet3.getText().toString());
                        mask[3] = Integer.parseInt(txtMaskOctet4.getText().toString());

                        if (radioDefault.isChecked()) { //default
                            checkClass();
                            calculateDefaultMask();
                            maskLength = calculateMaskLength(mask, maskLength);
                            System.out.println(ipClass);

                            if (maskLength < defaultMaskLength) {
                                //error
                            } else {
                                calculateSubNetworks();
                                int[] prevAddress = ip;
                                prevAddress[3]--;
                                ArrayList<String> networks = new ArrayList<>();
                                for (int i = 0; i < numOfNetworks; i++) {
                                    SubNetwork net = new SubNetwork(mask, prevAddress);
                                    networks.add(net.toString());
                                    Log.w("NET", net.toString());
                                    prevAddress = net.broadcast;
                                }

                                setList(networks);

                            }
                        } else { //custom

                            if (txtNumOfSubNet.getText().toString().isEmpty())
                                txtNumOfSubNet.setError("provide a number");
                            else {
                                txtNumOfSubNet.setError(null);
                                if (txtHostsPerNetwork.getText().toString().isEmpty()) {
                                    txtHostsPerNetwork.setError("!");

                                } else {
                                    numOfNetworks = Integer.parseInt(txtNumOfSubNet.getText().toString());
                                    String[] stringHosts = txtHostsPerNetwork.getText().toString().split(",");
                                    Log.w("HOSTS", stringHosts.length + "");
                                    if (stringHosts.length != numOfNetworks) {
                                        txtHostsPerNetwork.setError("error");
                                    } else {
                                        txtHostsPerNetwork.setError(null);

                                        int numOfHosts = 0;

                                        int[] hosts = new int[stringHosts.length];
                                        for (int i = 0; i < hosts.length; i++) {
                                            hosts[i] = Integer.parseInt(stringHosts[i].trim());
                                            numOfHosts += findNumOfHosts(hosts[i]);
                                        }

                                        int numOfHostsInNetwork = (int) Math.pow(2, (32 - maskLength)) - 2;

                                        if (numOfHosts > numOfHostsInNetwork) {
                                            txtHostsPerNetwork.setError("not enough hosts in the network");
                                        } else {

                                            txtHostsPerNetwork.setError(null);

                                            Arrays.sort(hosts);
                                            ArrayList<String> array = new ArrayList<>();
                                            int[] prevBroadcast = {ip[0], ip[1], ip[2], ip[3] - 1};
                                            for (int i = numOfNetworks - 1; i >= 0; i--) {
                                                int[] currentMask = calculateMask(hosts[i]);
                                                Log.w("CURRENT MASK", currentMask[0] + "." + currentMask[1] + "." + currentMask[2] + "." + currentMask[3]);
                                                SubNetwork sub = new SubNetwork(currentMask, prevBroadcast);
                                                array.add(sub.toString());
                                                prevBroadcast = sub.getBroadcast();
                                            }

                                            setList(array);
                                        }

                                    }
                                }
                            }
                        }
                    }

                }
            }

            private int[] calculateMask(int hosts) {
                int[] mask = new int[] {0, 0, 0, 0};
                int counter = 0;
                int powerOfTwo = 1;
                while (powerOfTwo < hosts+2) {
                    counter++;
                    powerOfTwo *= 2;
                }

                int i = 3;
                for (int j = 0; j < counter; j++) {
                    if (j == 255) i--;
                    mask[i] = (mask[i]+1)*2 -1;
                }
                for (int j = 0; j < 4; j++) {
                    mask[j] = 255-mask[j];
                }

                return mask;
            }
        });
    }

    public int findNumOfHosts(int numOfHosts) {
        int counter = 0;
        int powerOfTwo = 1;
        while (powerOfTwo < numOfHosts+2) {
            counter++;
            powerOfTwo *= 2;
        }

        return counter;
    }

    private  int calculateMaskLength(int[] mask, int length) {
        length = 0;
        for (int m: mask) {
            switch(m) {
                case 128:
                    length+= 1;
                    break;
                case 192:
                    length+= 2;
                    break;
                case 224:
                    length+= 3;
                    break;
                case 240:
                    length+= 4;
                    break;
                case 248:
                    length+= 5;
                    break;
                case 252:
                    length+= 6;
                    break;
                case 254:
                    length+= 7;
                    break;
                case 255:
                    length+= 8;
                    break;
            }
        }
        return length;
    }

    private void checkClass() {
        if (ip[0] < 128 && ip[0] > 0) ipClass = 'A';
        else {
            if (ip[0] < 192 ) ipClass = 'B';
            else {
                if (ip[0] < 224) ipClass = 'C';
                else ipClass = '?';
            }
        }
    }

    private void calculateSubNetworks() {
        numOfNetworks = (int) Math.pow(2, maskLength-defaultMaskLength);
        numOfHosts = (int) Math.pow(2, 32-maskLength)-2;
    }

    private  void calculateDefaultMask() {


        switch(ipClass) {
            case 'C':
                defaultMask[2] = 255;
            case 'B':
                defaultMask[1] = 255;
            case 'A':
                defaultMask[0] = 255;
                break;
        }
        defaultMaskLength = calculateMaskLength(defaultMask, defaultMaskLength);
    }

    private void setList(ArrayList<String> list) {
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
        listNetworks.setAdapter(adapter);
    }

}
