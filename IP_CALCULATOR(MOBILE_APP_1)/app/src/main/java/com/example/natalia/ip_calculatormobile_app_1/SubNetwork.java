package com.example.natalia.ip_calculatormobile_app_1;


public class SubNetwork {

    int[] ip = new int[4];
    int[] firstAddress = new int[4];
    int[] lastAddress = new int[4];
    int[] broadcast = new int[4];
    int[] inverseMask = new int[4];
    int[] mask;

    public SubNetwork(int[] mask, int[] prevIp) {

        this.mask = mask;
        calculateInverseMask(mask);
        calculateIP(prevIp);
        calculateBroadcast(mask);
        calculateFirstAndLastAddresses();

    }

    private void calculateInverseMask(int[] mask) {
        for (int i = 0; i < 4; i++) {
            inverseMask[i] = 255 - mask[i];
        }
    }

    private void calculateFirstAndLastAddresses() {
        for (int i = 0; i < 4; i++) {
            firstAddress[i] = ip[i];
            lastAddress[i] = broadcast[i];
        }

        firstAddress[3]++;
        lastAddress[3]--;
    }

    private void calculateBroadcast(int[] mask) {
        for (int i = 0; i < 4; i++) {
            broadcast[i] = ip[i] | inverseMask[i];
        }

    }

    private void calculateIP(int[] prevIp) {
        for (int i = 0; i < 4; i++) {
            ip[i] = prevIp[i];
        }
        if (ip[3] < 255) {
            ip[3]++;
        } else {
            ip[3] = 0;
            if (ip[2] < 255) {
                ip[2]++;
            } else {
                if (ip[1] < 255) {
                    ip[1]++;
                } else {
                    ip[1] = 0;
                    ip[0]++;
                }
            }
        }
    }

    public int[] getBroadcast() {
        return broadcast;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("IP: ");
        for (int i: ip) {
            sb.append(i);
            sb.append(".");
        }
        sb.append("\r\nFirst Address: ");
        for (int i: firstAddress) {
            sb.append(i);
            sb.append(".");
        }
        sb.append("\r\nLast Address: ");
        for (int i: lastAddress) {
            sb.append(i);
            sb.append(".");
        }
        sb.append("\r\nBroadcast: ");
        for (int i: broadcast) {
            sb.append(i);
            sb.append(".");
        }
        sb.append("\r\nMask:");
        for (int i: mask) {
            sb.append(i);
            sb.append(".");
        }

        return sb.toString();
    }
}