﻿namespace ShiftedAlphabet
{
    partial class AboutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblCreator = new System.Windows.Forms.Label();
            this.lblReason1 = new System.Windows.Forms.Label();
            this.lblReason3 = new System.Windows.Forms.Label();
            this.lblReason2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.White;
            this.lblName.Location = new System.Drawing.Point(78, 48);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(240, 24);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Advanced Shifted Alphabet";
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.ForeColor = System.Drawing.Color.White;
            this.lblVersion.Location = new System.Drawing.Point(132, 72);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(139, 24);
            this.lblVersion.TabIndex = 1;
            this.lblVersion.Text = "VERSION: 2.1.4";
            // 
            // lblCreator
            // 
            this.lblCreator.AutoSize = true;
            this.lblCreator.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreator.ForeColor = System.Drawing.Color.White;
            this.lblCreator.Location = new System.Drawing.Point(60, 142);
            this.lblCreator.Name = "lblCreator";
            this.lblCreator.Size = new System.Drawing.Size(285, 24);
            this.lblCreator.TabIndex = 3;
            this.lblCreator.Text = "CREATED BY DOMANOVA NATALIA";
            // 
            // lblReason1
            // 
            this.lblReason1.AutoSize = true;
            this.lblReason1.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReason1.ForeColor = System.Drawing.Color.White;
            this.lblReason1.Location = new System.Drawing.Point(43, 173);
            this.lblReason1.Name = "lblReason1";
            this.lblReason1.Size = new System.Drawing.Size(328, 24);
            this.lblReason1.TabIndex = 4;
            this.lblReason1.Text = "FOR WREXHAM GLYNDWR UNIVERSITY";
            // 
            // lblReason3
            // 
            this.lblReason3.AutoSize = true;
            this.lblReason3.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReason3.ForeColor = System.Drawing.Color.White;
            this.lblReason3.Location = new System.Drawing.Point(97, 226);
            this.lblReason3.Name = "lblReason3";
            this.lblReason3.Size = new System.Drawing.Size(214, 24);
            this.lblReason3.TabIndex = 5;
            this.lblReason3.Text = "APPLIED PROGRAMMING";
            // 
            // lblReason2
            // 
            this.lblReason2.AutoSize = true;
            this.lblReason2.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReason2.ForeColor = System.Drawing.Color.White;
            this.lblReason2.Location = new System.Drawing.Point(157, 198);
            this.lblReason2.Name = "lblReason2";
            this.lblReason2.Size = new System.Drawing.Size(84, 24);
            this.lblReason2.TabIndex = 6;
            this.lblReason2.Text = "COM 526";
            // 
            // AboutForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkCyan;
            this.ClientSize = new System.Drawing.Size(417, 307);
            this.Controls.Add(this.lblReason2);
            this.Controls.Add(this.lblReason3);
            this.Controls.Add(this.lblReason1);
            this.Controls.Add(this.lblCreator);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.lblName);
            this.Name = "AboutForm";
            this.Text = "AboutForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblCreator;
        private System.Windows.Forms.Label lblReason1;
        private System.Windows.Forms.Label lblReason3;
        private System.Windows.Forms.Label lblReason2;
    }
}