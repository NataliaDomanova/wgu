﻿using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Data.OleDb;
using System.Data;

namespace ShiftedAlphabet
{
    public partial class MainForm : Form
    {
        private int _idAgent;
        private bool _isEncode = true; //true - encoding, false - decoding
        private Alphabet[] _myAlphabets;
        private string _myKeyword;
        private int[] _myEncodedKeyword = new int[3];
        private SqlConnection _connection;

        public MainForm(string encodedKeyword, string keyword, int idAgent, SqlConnection connection)
        {
            InitializeComponent();

            this.Text = "Encoder/Decoder";
            _connection = connection;
           
            string[] parts = encodedKeyword.Split('.');
            for (int i = 0; i < 3; i++)
            {
                _myEncodedKeyword[i] = Int32.Parse(parts[i]);
            }
            if (keyword != "")
            {
                _myKeyword = keyword;

            } else
            {
                _myKeyword = DecodeKeyword(parts[0], parts[1], parts[2]);
            }

            Console.WriteLine(_myKeyword);

            _idAgent = idAgent;
            _myAlphabets = new Alphabet[_myKeyword.Length];
            for (int i = 0; i < _myKeyword.Length; i++)
            {
                Alphabet al = new Alphabet(_myKeyword[i]);
                _myAlphabets[i] = al;
            }


            changeTab(true);
            btnTabEncode.BackColor = Color.DarkCyan; ;
            btnTabEncode.ForeColor = Color.White;
            btnTabDecode.BackColor = Color.White;
            btnTabDecode.ForeColor = Color.DarkSlateGray;
            btnTabEncode.FlatAppearance.BorderSize = 0;
            btnTabDecode.FlatAppearance.BorderSize = 0;
           
            SetMyKeyword();
        }

        public static String DecodeKeyword(string p, string l, string w)
        {
            int poem = Int32.Parse(p);
            Console.WriteLine("Poem " + poem);
            int line = Int32.Parse(l);

            Console.WriteLine("line " + line);
            int word = Int32.Parse(w);

            Console.WriteLine("Word " + word);
            string fileName = "";
            switch (poem)
            {
                case 1:
                    fileName = "POEM1-All the Worlds a Stage by William Shakespeare.xlsx";
                    break;
                case 2:
                    fileName = "POEM2-I Wandered Lonely As A Cloud by William Wordsworth.xlsx";
                    break;
                case 3:
                    fileName = "POEM3-The Tiger by William Blake.xlsx";
                    break;
                default:
                    return "";

            }
            DirectoryInfo dirInfo = new DirectoryInfo(Directory.GetCurrentDirectory());
            String temp = dirInfo.Parent.Parent.Parent.FullName;
            String excelPath = Path.Combine(temp, "Resources", fileName);
            string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;" +
                      "Data Source='" + excelPath +
                      "';Extended Properties=\"Excel 12.0;HDR=YES;\"";

            using (OleDbConnection connection = new OleDbConnection(connectionString))
            {
                DataTable sheet = new DataTable();
                connection.Open();
                string selectSql = @"SELECT * FROM [Sheet1$]";
                using (OleDbDataAdapter adapter = new OleDbDataAdapter(selectSql, connection))
                {
                    adapter.Fill(sheet);
                    int maxRow = sheet.Rows.Count;
                    if (line > maxRow + 1 || line == 0) return "";
                    string row = "";
                    if (line == 1)
                    {
                        row = sheet.Columns[0].ColumnName;
                    }
                    else
                    {
                        row = sheet.Rows[line - 2].ItemArray[0].ToString();
                    }
                    string[] words = row.Split(' ');
                    int maxWord = words.Length;
                    Console.WriteLine("MAX word: " + maxWord);
                    if (word > maxWord || word == 0)return "";
                    
                    Console.WriteLine(words[word-1]);
                    StringBuilder sb = new StringBuilder();
                    foreach (char c in words[word-1])
                    {
                        if (!char.IsPunctuation(c))
                        {
                            sb.Append(c);
                        }
                    }
                    return sb.ToString();
                }
            }
        }

        private void AddEncodedMessageRecord()
        {
            string date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fff");
            StringBuilder sb = new StringBuilder();
            sb.Append("INSERT INTO encoded_message (id_agent, message, code_date)");
            sb.Append("VALUES ("+_idAgent+", '"+txtOutputMessage.Text+"', '"+date+"');");
            using (var command = new SqlCommand(sb.ToString(), _connection))
            {
                command.ExecuteNonQuery();
            }
        }

        private void AddDecodedMessageRecord(string encodedKeyword)
        {
            string date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fff");
            StringBuilder sb = new StringBuilder();
            sb.Append("INSERT INTO decoded_message (id_agent, keyword, message, code_date)");
            sb.Append("VALUES (" + _idAgent+", '"+encodedKeyword+"', '" + txtOutputMessage.Text + "', '" + date + "');");
            using (var command = new SqlCommand(sb.ToString(), _connection))
            {
                command.ExecuteNonQuery();
            }
        }

        private bool RecordExists()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT count(message) ");
            if (_isEncode)
            {
                sb.Append("FROM encoded_message ");
                sb.Append("WHERE ");
            } else
            {
                sb.Append("FROM decoded_message ");
                sb.Append("WHERE keyword = '"+txtKeyword1.Text+"' AND ");
            }
            sb.Append("id_agent = " + _idAgent + " ");
            sb.Append("AND message = '" + txtOutputMessage.Text + "';");
            Console.WriteLine(sb.ToString());

            using (var command = new SqlCommand(sb.ToString(), _connection))
            {
                int counter = (int) command.ExecuteScalar();
                Console.WriteLine("NUM OF REC: " + counter);
                if (counter == 0) return false;
                return true;
            }
        }
       
   
        private void btnDo_Click(object sender, EventArgs e)
        {
           if (_isEncode)
            {
                if (!txtInputMessage.Text.Equals(""))
                {
                    if (ContainsLetters(txtInputMessage.Text)) {
                        txtOutputMessage.Text = EncodeMessage(txtInputMessage.Text);
                        if (!RecordExists()) { AddEncodedMessageRecord(); }
                    } else
                    {
                        string message = "You can encode only Latin letters";
                        string title = "Information";
                        MessageBox.Show(message, title);
                    }
                }
                else txtOutputMessage.Text = "";
            } else
            {
                if (!txtInputMessage.Text.Equals("") && !txtKeyword1.Text.Equals("")
                    &&!txtKeyword2.Text.Equals("") && !txtKeyword3.Text.Equals(""))
                {
                    string keyword = DecodeKeyword(txtKeyword1.Text, txtKeyword2.Text, txtKeyword3.Text);
                    string encodedKeyword = getEncodedKeywordAsString(txtKeyword1.Text, txtKeyword2.Text, txtKeyword3.Text);
                    if (keyword != "")
                    {
                        if (ContainsLetters(txtInputMessage.Text)) {
                            txtOutputMessage.Text = DecodeMessage(txtInputMessage.Text, keyword);
                            if (!RecordExists()) { AddDecodedMessageRecord(encodedKeyword); }
                            lblError.Visible = false;
                        } else
                        {
                            string message = "You can decode only Latin letters";
                            string title = "Information";
                            MessageBox.Show(message, title);
                        }
                    } else
                    {
                        lblError.Visible = true;
                    }
                }
                else txtOutputMessage.Text = "";
            }
        }

        private string getEncodedKeywordAsString(string p, string l, string w)
        {
            StringBuilder sb = new StringBuilder();

            if ( Int32.Parse(p) - 10 >= 0)
            {
                sb.Append(p);
            } else
            {
                sb.Append("0");
                sb.Append(p);
            }
            sb.Append(".");
            if (Int32.Parse(l) - 10 >= 0)
            {
                sb.Append(l);
            }
            else
            {
                sb.Append("0");
                sb.Append(l);
            }
            sb.Append(".");
            if (Int32.Parse(w) - 10 >= 0)
            {
                sb.Append(w);
            }
            else
            {
                sb.Append("0");
                sb.Append(w);
            }

            return sb.ToString();
        }

        private string EncodeMessage(string message)
        {
            int i = 0;
            string encodedMessage = "";
            foreach(char c in message)
            {
                
                if (IsLatinLetter(c))
                {
                    encodedMessage += _myAlphabets[i].EncodeCharacter(c);
                    i++;
                    if (i == _myAlphabets.Length) i = 0;
                } else
                {
                    if (Char.IsWhiteSpace(c)) encodedMessage += c;
                }
            }

            return encodedMessage;
        }

        private string DecodeMessage(string message, string keyword)
        {
            Alphabet[] alphabets = new Alphabet[keyword.Length];
            for (int j = 0; j < keyword.Length; j++)
            {
                Alphabet al = new Alphabet(keyword[j]);
                alphabets[j] = al;
            }

            int i = 0;
            string decodedMessage = "";
            foreach (char c in message)
            {
                
                if (IsLatinLetter(c))
                {
                    decodedMessage += alphabets[i].DecodeCharacter(c);
                    i++;
                    if (i == alphabets.Length) i = 0;
                }
                else
                {
                    if (Char.IsWhiteSpace(c)) decodedMessage += c; 
                }
            }

            return decodedMessage;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveMessage();
        }

        private void btnTabDecode_Click(object sender, EventArgs e)
        {
            changeTab(false);
            btnTabDecode.BackColor = Color.DarkCyan;
            btnTabDecode.ForeColor = Color.White;
            btnTabEncode.BackColor = Color.White;
            btnTabEncode.ForeColor = Color.DarkSlateGray;
            
        }

        private void changeTab(bool isEncode)
        {
            txtKeyword1.ReadOnly = isEncode;
            txtKeyword2.ReadOnly = isEncode;
            txtKeyword3.ReadOnly = isEncode;
            _isEncode = isEncode;
            txtInputMessage.Text = "";
            txtOutputMessage.Text = "";
            if (isEncode)
            {
                btnDo.Text = "ENCODE";
                SetMyKeyword();
            } else
            {
                btnDo.Text = "DECODE";
                txtKeyword1.Text = "";
                txtKeyword2.Text = "";
                txtKeyword3.Text = "";
            }
        }

        private void btnTabEncode_Click(object sender, EventArgs e)
        {
            changeTab(true);
            btnTabEncode.BackColor = Color.DarkCyan; ;
            btnTabEncode.ForeColor = Color.White;
            btnTabDecode.BackColor = Color.White;
            btnTabDecode.ForeColor = Color.DarkSlateGray;
        }

        private void btnStatistics_Click(object sender, EventArgs e)
        {
            var statistics = new Statistics(_connection);
            statistics.Show();
        }

        public static void exitConfirmation()
        {
            if (MessageBox.Show("Are you sure you want to close?", "Infomate", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Environment.Exit(0);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            exitConfirmation();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog.Title = "Open text file";
            openFileDialog.Filter = "Text File | *.txt";
            openFileDialog.FileName = "";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                StreamReader reader = new StreamReader(openFileDialog.OpenFile());
                string fileText = reader.ReadToEnd();
                txtInputMessage.Text = fileText;
                reader.Close();
            }

            txtOutputMessage.Text = "";
        }

        private void SaveMessage()
        {
            if (txtOutputMessage.Text != "")
            {
                saveFileDialog.Title = "Save text file";
                saveFileDialog.Filter = "Text File | *.txt";
                string date = DateTime.Now.ToString("yyyyMMddHHmmss");
                saveFileDialog.FileName = date + ".txt";

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter writer = new StreamWriter(saveFileDialog.OpenFile());
                    writer.Write(txtOutputMessage.Text);
                    writer.Flush();
                    writer.Close();
                }
            }
        }

        private bool ContainsLetters(string text)
        {
            foreach (char c in text)
            {
                if (IsLatinLetter(c)) return true;
            }
            return false;
        }

        public static bool IsLatinLetter(char c)
        {
            return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'); 
        }

        private void SetMyKeyword()
        {
            txtKeyword1.Text = _myEncodedKeyword[0]+"";
            txtKeyword2.Text = _myEncodedKeyword[1] + "";
            txtKeyword3.Text = _myEncodedKeyword[2] + "";
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveMessage();
        }

        private void btnTabDecode_MouseEnter(object sender, EventArgs e)
        {
            
        }

        private void btnTabDecode_MouseLeave(object sender, EventArgs e)
        {
            
        }

        private void txtKeyword1_TextChanged(object sender, EventArgs e)
        {
            checkTxtKeyword(txtKeyword1);
        }

        private void txtKeyword2_TextChanged(object sender, EventArgs e)
        {
            checkTxtKeyword(txtKeyword2);
        }

        private void txtKeyword3_TextChanged(object sender, EventArgs e)
        {
            checkTxtKeyword(txtKeyword3);
        }

        private void checkTxtKeyword(TextBox txt)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txt.Text, "[^0-9]"))
            {
                lblError.Visible = true;
                txt.Text = "";
            }
            else
            {
                lblError.Visible = false;
            }
        }

        public static void showAboutForm()
        {
            var aboutForm = new AboutForm();
            aboutForm.Show();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showAboutForm();
        }
    }
}
