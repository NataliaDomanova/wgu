﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShiftedAlphabet
{
    
    class Alphabet
    {
        private int _shiftValue;

        public Alphabet(char c)
        {
            int value = (int)c;
            if (value >= 65 && value <= 90)
            {
                _shiftValue = value - 65;
            } else
            {
                _shiftValue = value - 97;
            }
        }

        public char EncodeCharacter(char c)
        {
            int value = (int)c;
            int newValue = value + _shiftValue;
            if (value<= 90)
            {
                if (newValue> 90)
                {
                    newValue = 64 + (newValue - 90);
                }
            } else
            {
                if (value <= 122)
                {
                    if (newValue > 122)
                    {
                        newValue = 96 + (newValue - 122);
                    }
                }
            }
            char newC = (char)newValue;
            return newC;
        }

        public char DecodeCharacter(char c)
        {
            int value = (int)c;
            int newValue = value - _shiftValue;
            if (value <= 90)
            {
                if (newValue < 65)
                {
                    newValue = 91 - (65 - newValue);
                }
            } else
            {
                if (value <= 122)
                {
                    if (newValue < 97)
                    {
                        newValue = 123 - (97 - newValue);
                    }
                }
            }

            char newC = (char) newValue;
            return newC;
        }
        
    }
}
