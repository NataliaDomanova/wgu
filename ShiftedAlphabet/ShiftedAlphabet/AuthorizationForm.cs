﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Text;
using System.IO;

namespace ShiftedAlphabet
{
    public partial class AuthorizationForm : Form
    {
        private SqlConnection _connection;

        public AuthorizationForm()
        {
            InitializeComponent();
            this.Text = "Shifted Alphabet Application";
           // initFont();
        
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ConnectToAzure()
        {
            try { 
                var cb = new SqlConnectionStringBuilder();
                cb.DataSource = "glyndwr.database.windows.net";
                cb.UserID = "natalia_domanova";
                cb.Password = "l1w32h76d49n38c72T!&";
                cb.InitialCatalog = "ShiftedAplhabet";
                _connection = new SqlConnection(cb.ConnectionString);
                _connection.Open();
            } catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            
        }

        private void LogIn()
        {
            bool agentExist = false;
            int idAgent = 0;
            string encodedKeyword = "";

            ConnectToAzure();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT id_agent FROM Agent ");
            sb.Append("WHERE username='" + txtUsername.Text + "' ");
            sb.Append("AND ");
            sb.Append("password = '" + txtPassword.Text + "';");
            string query = sb.ToString();

            using (var command = new SqlCommand(query, _connection))
            {
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        agentExist = true;
                        idAgent = reader.GetInt32(0);
                    }

                }
                if (agentExist)
                {
                    Console.WriteLine("There is such a record");
                    Console.WriteLine("Agent ID is " + idAgent);

                    //send another query to check if agent
                    //already has a keyword
                    string date = DateTime.Now.ToString("dd.MM.yyyy");
                    sb.Clear();
                    sb.Append("SELECT word FROM Keyword ");
                    sb.Append("WHERE id_agent=" + idAgent + " ");
                    sb.Append("AND ");
                    sb.Append("valid_date ='" + date + "';");


                    using (var checkKeywordCommand = new SqlCommand(sb.ToString(), _connection))
                    {
                        using (var checkKeywordReader = checkKeywordCommand.ExecuteReader())
                        {
                            if (checkKeywordReader.Read())
                            {
                                //key exists, do not show choose key form
                                encodedKeyword = checkKeywordReader.GetString(0);
                                var mainForm = new MainForm(encodedKeyword, "", idAgent, _connection);
                                mainForm.FormClosed += new FormClosedEventHandler(mainForm_Closed);
                                mainForm.Show();
                                this.Hide();

                            }
                            else
                            {
                                //open choose keyword form and add record
                                var getKeywordForm = new GetKeywordForm(idAgent, _connection);
                                getKeywordForm.FormClosed += new FormClosedEventHandler(getKeywordForm_Closed);
                                getKeywordForm.Show();
                                this.Hide();
                            }
                        }
                    }
                }
                else
                {
                    lblError.Text = "INCORRECT USERNAME OR PASSWORD";
                }
            }

        }

        private void btnLogIn_Click(object sender, EventArgs e)
        {
            LogIn();
        }

        private void getKeywordForm_Closed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }

        private void mainForm_Closed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                LogIn();
            }
        }
    }
}
