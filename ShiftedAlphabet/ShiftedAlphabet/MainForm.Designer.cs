﻿namespace ShiftedAlphabet
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTabEncode = new System.Windows.Forms.Button();
            this.btnTabDecode = new System.Windows.Forms.Button();
            this.txtInputMessage = new System.Windows.Forms.RichTextBox();
            this.lblKeyword = new System.Windows.Forms.Label();
            this.txtKeyword1 = new System.Windows.Forms.TextBox();
            this.btnDo = new System.Windows.Forms.Button();
            this.txtOutputMessage = new System.Windows.Forms.RichTextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblInput = new System.Windows.Forms.Label();
            this.lblOutput = new System.Windows.Forms.Label();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.btnStatistics = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.txtKeyword2 = new System.Windows.Forms.TextBox();
            this.txtKeyword3 = new System.Windows.Forms.TextBox();
            this.lblError = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnTabEncode
            // 
            this.btnTabEncode.BackColor = System.Drawing.Color.DarkCyan;
            this.btnTabEncode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTabEncode.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTabEncode.ForeColor = System.Drawing.Color.White;
            this.btnTabEncode.Location = new System.Drawing.Point(0, 24);
            this.btnTabEncode.Margin = new System.Windows.Forms.Padding(0);
            this.btnTabEncode.Name = "btnTabEncode";
            this.btnTabEncode.Size = new System.Drawing.Size(253, 31);
            this.btnTabEncode.TabIndex = 1;
            this.btnTabEncode.Text = "ENCODE";
            this.btnTabEncode.UseVisualStyleBackColor = false;
            this.btnTabEncode.Click += new System.EventHandler(this.btnTabEncode_Click);
            // 
            // btnTabDecode
            // 
            this.btnTabDecode.BackColor = System.Drawing.Color.White;
            this.btnTabDecode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTabDecode.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTabDecode.Location = new System.Drawing.Point(251, 24);
            this.btnTabDecode.Name = "btnTabDecode";
            this.btnTabDecode.Size = new System.Drawing.Size(251, 31);
            this.btnTabDecode.TabIndex = 2;
            this.btnTabDecode.Text = "DECODE";
            this.btnTabDecode.UseVisualStyleBackColor = false;
            this.btnTabDecode.Click += new System.EventHandler(this.btnTabDecode_Click);
            this.btnTabDecode.MouseEnter += new System.EventHandler(this.btnTabDecode_MouseEnter);
            this.btnTabDecode.MouseLeave += new System.EventHandler(this.btnTabDecode_MouseLeave);
            // 
            // txtInputMessage
            // 
            this.txtInputMessage.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInputMessage.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.txtInputMessage.Location = new System.Drawing.Point(13, 97);
            this.txtInputMessage.Name = "txtInputMessage";
            this.txtInputMessage.Size = new System.Drawing.Size(476, 96);
            this.txtInputMessage.TabIndex = 4;
            this.txtInputMessage.Text = "";
            // 
            // lblKeyword
            // 
            this.lblKeyword.AutoSize = true;
            this.lblKeyword.Font = new System.Drawing.Font("Trebuchet MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKeyword.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblKeyword.Location = new System.Drawing.Point(12, 206);
            this.lblKeyword.Name = "lblKeyword";
            this.lblKeyword.Size = new System.Drawing.Size(82, 20);
            this.lblKeyword.TabIndex = 5;
            this.lblKeyword.Text = "KEYWORD:";
            // 
            // txtKeyword1
            // 
            this.txtKeyword1.BackColor = System.Drawing.Color.White;
            this.txtKeyword1.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKeyword1.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.txtKeyword1.Location = new System.Drawing.Point(91, 206);
            this.txtKeyword1.Name = "txtKeyword1";
            this.txtKeyword1.Size = new System.Drawing.Size(36, 23);
            this.txtKeyword1.TabIndex = 6;
            this.txtKeyword1.TextChanged += new System.EventHandler(this.txtKeyword1_TextChanged);
            // 
            // btnDo
            // 
            this.btnDo.BackColor = System.Drawing.Color.DarkCyan;
            this.btnDo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDo.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDo.ForeColor = System.Drawing.Color.White;
            this.btnDo.Location = new System.Drawing.Point(231, 205);
            this.btnDo.Name = "btnDo";
            this.btnDo.Size = new System.Drawing.Size(117, 25);
            this.btnDo.TabIndex = 9;
            this.btnDo.Text = "ENCODE";
            this.btnDo.UseVisualStyleBackColor = false;
            this.btnDo.Click += new System.EventHandler(this.btnDo_Click);
            // 
            // txtOutputMessage
            // 
            this.txtOutputMessage.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOutputMessage.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.txtOutputMessage.Location = new System.Drawing.Point(13, 277);
            this.txtOutputMessage.Name = "txtOutputMessage";
            this.txtOutputMessage.Size = new System.Drawing.Size(476, 102);
            this.txtOutputMessage.TabIndex = 12;
            this.txtOutputMessage.Text = "";
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.DarkCyan;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(372, 387);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(117, 27);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "SAVE";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblInput
            // 
            this.lblInput.AutoSize = true;
            this.lblInput.Font = new System.Drawing.Font("Trebuchet MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInput.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblInput.Location = new System.Drawing.Point(191, 70);
            this.lblInput.Name = "lblInput";
            this.lblInput.Size = new System.Drawing.Size(115, 20);
            this.lblInput.TabIndex = 3;
            this.lblInput.Text = "INPUT MESSAGE";
            // 
            // lblOutput
            // 
            this.lblOutput.AutoSize = true;
            this.lblOutput.Font = new System.Drawing.Font("Trebuchet MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutput.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblOutput.Location = new System.Drawing.Point(189, 250);
            this.lblOutput.Name = "lblOutput";
            this.lblOutput.Size = new System.Drawing.Size(130, 20);
            this.lblOutput.TabIndex = 11;
            this.lblOutput.Text = "OUTPUT MESSAGE";
            // 
            // btnStatistics
            // 
            this.btnStatistics.BackColor = System.Drawing.Color.DarkCyan;
            this.btnStatistics.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStatistics.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStatistics.ForeColor = System.Drawing.Color.White;
            this.btnStatistics.Location = new System.Drawing.Point(13, 387);
            this.btnStatistics.Name = "btnStatistics";
            this.btnStatistics.Size = new System.Drawing.Size(150, 27);
            this.btnStatistics.TabIndex = 13;
            this.btnStatistics.Text = "SHOW STATISTICS";
            this.btnStatistics.UseVisualStyleBackColor = false;
            this.btnStatistics.Click += new System.EventHandler(this.btnStatistics_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.White;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(504, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // txtKeyword2
            // 
            this.txtKeyword2.BackColor = System.Drawing.Color.White;
            this.txtKeyword2.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKeyword2.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.txtKeyword2.Location = new System.Drawing.Point(133, 206);
            this.txtKeyword2.Name = "txtKeyword2";
            this.txtKeyword2.Size = new System.Drawing.Size(36, 23);
            this.txtKeyword2.TabIndex = 7;
            this.txtKeyword2.TextChanged += new System.EventHandler(this.txtKeyword2_TextChanged);
            // 
            // txtKeyword3
            // 
            this.txtKeyword3.BackColor = System.Drawing.Color.White;
            this.txtKeyword3.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKeyword3.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.txtKeyword3.Location = new System.Drawing.Point(175, 206);
            this.txtKeyword3.Name = "txtKeyword3";
            this.txtKeyword3.Size = new System.Drawing.Size(36, 23);
            this.txtKeyword3.TabIndex = 8;
            this.txtKeyword3.TextChanged += new System.EventHandler(this.txtKeyword3_TextChanged);
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.DarkRed;
            this.lblError.Location = new System.Drawing.Point(84, 232);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(138, 18);
            this.lblError.TabIndex = 10;
            this.lblError.Text = "INCORRECT KEYWORD";
            this.lblError.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(504, 436);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.txtKeyword3);
            this.Controls.Add(this.txtKeyword2);
            this.Controls.Add(this.btnStatistics);
            this.Controls.Add(this.lblOutput);
            this.Controls.Add(this.lblInput);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtOutputMessage);
            this.Controls.Add(this.btnDo);
            this.Controls.Add(this.txtKeyword1);
            this.Controls.Add(this.lblKeyword);
            this.Controls.Add(this.txtInputMessage);
            this.Controls.Add(this.btnTabDecode);
            this.Controls.Add(this.btnTabEncode);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTabEncode;
        private System.Windows.Forms.Button btnTabDecode;
        private System.Windows.Forms.RichTextBox txtInputMessage;
        private System.Windows.Forms.Label lblKeyword;
        private System.Windows.Forms.TextBox txtKeyword1;
        private System.Windows.Forms.Button btnDo;
        private System.Windows.Forms.RichTextBox txtOutputMessage;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblInput;
        private System.Windows.Forms.Label lblOutput;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.Button btnStatistics;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.TextBox txtKeyword2;
        private System.Windows.Forms.TextBox txtKeyword3;
        private System.Windows.Forms.Label lblError;
    }
}