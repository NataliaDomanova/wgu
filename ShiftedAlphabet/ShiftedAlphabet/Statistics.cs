﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using System.Collections.Generic;

namespace ShiftedAlphabet
{
    public partial class Statistics : Form
    {
        SqlConnection _connection;
        Dictionary<string, string> keywords = new Dictionary<string, string>();

        public Statistics(SqlConnection connection)
        {
            InitializeComponent();

            this.Text = "Statistics";
            _connection = connection;

            dateStart.Checked = false;
            dateEnd.Checked = false;
            dateStart.MaxDate = System.DateTime.Today;
            dateEnd.MaxDate = System.DateTime.Today;

            SendQuery();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT username FROM Agent;");
            using (var command = new SqlCommand(sb.ToString(), _connection))
            {
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read()) {
                        string username = reader.GetString(0);
                        listUsers.Items.Add(username);
                    }
                }
            }
            sb.Clear();
            sb.Append("SELECT  word FROM Keyword;");
            using (var command = new SqlCommand(sb.ToString(), _connection))
            {
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string encodedKeyword = reader.GetString(0);
                        string[] parts = encodedKeyword.Split('.');
                        string keyword = MainForm.DecodeKeyword(parts[0], parts[1], parts[2]);
                        keywords[encodedKeyword] = keyword;
                        if (!listKeywords.Items.Contains(keyword))  listKeywords.Items.Add(keyword);
                    }
                }
            }

        }

        private void SendQuery()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT message as Message, code_date as Date, username as Agent, word as Encoded_Keyword ");
            sb.Append("FROM encoded_message, agent, keyword ");
            sb.Append("WHERE valid_date = convert(varchar(10), code_date, 104) ");
            sb.Append("AND keyword.id_agent = encoded_message.id_agent ");
            sb.Append("AND keyword.id_agent = agent.id_agent ");
            if (listUsers.SelectedItem != null)
                sb.Append("AND username = '" + listUsers.SelectedItem.ToString() + "' ");
            if (listKeywords.SelectedItem != null)
            {
                string keyword = listKeywords.SelectedItem.ToString();
                foreach (KeyValuePair<string, string> pair in keywords)
                {
                    if (pair.Value.Equals(keyword))
                    {
                        sb.Append("AND word = '" + pair.Key + "' ");
                    }
                }
               
            }
               
            if (dateStart.Checked && dateEnd.Checked)
                sb.Append("AND code_date >= '" + dateStart.Value + "' AND code_date <= '" + dateEnd.Value + "' ");
            if (dateStart.Checked && !dateEnd.Checked)
                sb.Append("AND code_date > '" + dateStart.Value + "' ");
            sb.Append("UNION ");
            sb.Append("SELECT message, code_date, username, keyword ");
            sb.Append("FROM decoded_message, agent ");
            sb.Append("WHERE decoded_message.id_agent = agent.id_agent ");

            if (listUsers.SelectedItem != null)
                sb.Append("AND username = '" + listUsers.SelectedItem.ToString() + "' ");
            if (listKeywords.SelectedItem != null)
            {
                string keyword = listKeywords.SelectedItem.ToString();
                foreach (KeyValuePair<string, string> pair in keywords)
                {
                    if (pair.Value.Equals(keyword))
                    {
                        sb.Append("AND keyword = '" + pair.Key + "' ");
                    }
                }

            }
            if (dateStart.Checked && dateEnd.Checked)
                sb.Append("AND code_date >= '" + dateStart.Value + "' AND code_date <= '" + dateEnd.Value.AddDays(1) + "' ");
            if (dateStart.Checked && !dateEnd.Checked)
                sb.Append("AND code_date > '" + dateStart.Value + "' ");


            sb.Append("ORDER BY code_date desc;");

            var adapter = new SqlDataAdapter(sb.ToString(), _connection);
            var commandBuilder = new SqlCommandBuilder(adapter);
            var dataSet = new DataSet();
            adapter.Fill(dataSet);
            dataGridView1.ReadOnly = true;
            dataGridView1.DataSource = dataSet.Tables[0];
            lblNumber.Text = dataSet.Tables[0].Rows.Count+"";
            if (dataSet.Tables[0].Rows.Count != 0)
            {
                txtMessage.Text = dataGridView1.Rows[0].Cells["Message"].Value.ToString();
            } else
            {
                txtMessage.Text = "";
            }
            }



        private void btnSearch_Click(object sender, EventArgs e)
        {
            SendQuery(); 
           
        }

        private void dateStart_ValueChanged(object sender, EventArgs e)
        {
            
            if (dateStart.Checked)
            {
                dateStart.CustomFormat = "yyyy-MM-dd";
                dateEnd.MinDate = dateStart.Value;
            } else
            {
                dateStart.CustomFormat = " ";
                dateEnd.Checked = false;
                dateEnd.CustomFormat = " ";
            }
        }

        private void dateEnd_ValueChanged(object sender, EventArgs e)
        {
            if (dateStart.Checked)
            {
                if (dateEnd.Checked)
                {
                    dateEnd.CustomFormat = "yyyy-MM-dd";
                    if (dateStart.Value > dateEnd.Value)
                    {
                        dateEnd.Value = dateStart.Value;
                    }
                } else
                {
                    dateEnd.CustomFormat = " ";
                }
            } else
            {
                dateEnd.Checked = false;
            }
           
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = dataGridView1.Rows[e.RowIndex];
                txtMessage.Text = row.Cells["Message"].Value.ToString();
            }
        }

        private void lblNumber_Click(object sender, EventArgs e)
        {

        }
    }
}
