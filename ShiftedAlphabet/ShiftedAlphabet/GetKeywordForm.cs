﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace ShiftedAlphabet
{
    public partial class GetKeywordForm : Form
    {
        private int _idAgent;
        private string _poemSubstring;
        private int _poemNumber;
        private SqlConnection _connection;
        private bool _changingPoem = false;

        public GetKeywordForm(int idAgent, SqlConnection connection)
        {
            InitializeComponent();
            this.Text = "Get Keyword";
            _idAgent = idAgent;
            _connection = connection;
        }

        private void lblKeyword_Click(object sender, EventArgs e)
        {

        }

        private void radio1_CheckedChanged(object sender, EventArgs e)
        {
            ChangePoem("POEM1-All the Worlds a Stage by William Shakespeare.xlsx");
            _poemNumber = 1;
        }

        private void ChangePoem(string fileName)
        {
            _changingPoem = true;
            txtPoem.SelectedText = "";
            txtKeyword.Text = "";

            DirectoryInfo dirInfo = new DirectoryInfo(Directory.GetCurrentDirectory());
            String temp = dirInfo.Parent.Parent.Parent.FullName;
            String excelPath = Path.Combine(temp, "Resources", fileName);
            string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;"+
                      "Data Source='" + excelPath +
                      "';Extended Properties=\"Excel 12.0;HDR=YES;\"";

            using (OleDbConnection connection = new OleDbConnection(connectionString))
            {
                DataTable sheet = new DataTable();
                connection.Open();
                string selectSql = @"SELECT * FROM [Sheet1$]";
                using (OleDbDataAdapter adapter = new OleDbDataAdapter(selectSql, connection))
                {
                    adapter.Fill(sheet);
                    string poem = sheet.Columns[0].ColumnName+"\r\n";
                    foreach (DataRow row in sheet.Rows)
                    {
                        poem += row[0]+"\r\n";
                    }
                    txtPoem.Text = poem;
                }
            }
        }

        public String EncodeKeyword() { 
            string poem = "0" + _poemNumber;

            string[] lines = _poemSubstring.Split('\n');
            
            int lineNum = lines.Length;
            string[] words = lines[lineNum - 1].Split(' ');
            Console.WriteLine("LINE: " + lines[lineNum - 1]);
            Console.WriteLine(words[words.Length - 1]);
            int wordNum = words.Length;
            string line = "";
            string word = "";
            if (lineNum - 10 >= 0) { line = lineNum + "";  }
            else { line = "0" + lineNum;  }
            if (wordNum - 10 >= 0) { word = wordNum + ""; }
            else { word = "0" + wordNum; }


            return poem + "." + line + "." + word;


        }

        private void radio2_CheckedChanged(object sender, EventArgs e)
        {
            ChangePoem("POEM2-I Wandered Lonely As A Cloud by William Wordsworth.xlsx");
            _poemNumber = 2;
        }

        private void radio3_CheckedChanged(object sender, EventArgs e)
        {
            ChangePoem("POEM3-The Tiger by William Blake.xlsx");
            _poemNumber = 3;
        }

        private void txtPoem_SelectionChanged(object sender, EventArgs e)
        {
            //add UpperCase allowance
            //adding added a bug :(

            if (_changingPoem)
            {
                _changingPoem = false;
                return;
            }

            string selectedText = txtPoem.SelectedText;
           
            bool onlyLetters = true;
            foreach (char c in selectedText)
            {
                if (!(c >= 'a' && c <= 'z')&&!(c >= 'A' && c <= 'Z'))
                {
                    onlyLetters = false;
                }
            }
           

            int startPos = txtPoem.SelectionStart;
            int endPos = startPos + txtPoem.SelectionLength;
            if (endPos > txtPoem.Text.Length - 1) endPos = txtPoem.Text.Length - 1;


            if (txtPoem.Text[startPos] == ' ')
            {
                startPos++;
                endPos++;
            }
            else
            {
                while ((startPos != 0) && (MainForm.IsLatinLetter(txtPoem.Text[startPos - 1])))
                {

                    startPos--;
                }
            }

            
            while ( (endPos != txtPoem.Text.Length-1) && ( MainForm.IsLatinLetter(txtPoem.Text[endPos])))
            {
                    endPos++;
            }
            

            if (onlyLetters)
            {
                txtKeyword.Text = txtPoem.Text.Substring(startPos, (endPos - startPos));
                Console.WriteLine("SUBSTRING: " + txtPoem.Text.Substring(0, startPos));
            } else
            {
                string substring = txtPoem.Text.Substring(startPos, (endPos - startPos));
                Console.WriteLine("SUBSTRING: " + txtPoem.Text.Substring(0, startPos));
                endPos = 0;
                for (int i = 0; i < substring.Length; i++)
                {
                    if (!(substring[i] >= 'a' && substring[i] <= 'z') 
                        && !(substring[i] >= 'A' && substring[i] <= 'Z'))
                    {
                        endPos = i;
                        break;
                    }
                }
                txtKeyword.Text = substring.Substring(0, endPos);
            }
            _poemSubstring = txtPoem.Text.Substring(0, startPos);

        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (!txtKeyword.Text.Equals(""))
            {
                string encodedKeyword = EncodeKeyword();
                string keyword = txtKeyword.Text;



                string date = DateTime.Now.ToString("dd.MM.yyyy");
                        StringBuilder sb = new StringBuilder();
                        sb.Append("INSERT INTO Keyword (word, id_agent, valid_date) ");
                        sb.Append("VALUES ('"+encodedKeyword+"', "+_idAgent+", '"+date+"');");
                        string query = sb.ToString();

                        using (var command = new SqlCommand(query, _connection))
                        {
                            command.ExecuteNonQuery();
                        }
                    
                

                var mainForm = new MainForm(encodedKeyword, keyword, _idAgent, _connection);
                mainForm.FormClosed += new FormClosedEventHandler(mainForm_Closed);
                mainForm.Show();
                this.Hide();
            }
        }

        private void mainForm_Closed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm.exitConfirmation();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm.showAboutForm();
        }
    }
}
