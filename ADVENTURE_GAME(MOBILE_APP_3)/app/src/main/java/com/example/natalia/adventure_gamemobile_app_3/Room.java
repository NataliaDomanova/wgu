package com.example.natalia.adventure_gamemobile_app_3;

import java.util.ArrayList;

public class Room {

    private String description;
    private int number;
    private ArrayList<Item> items;
    private int south;
    private int north;
    private int east;
    private int west;

    public void setEastDoor(Door eastDoor) {
        this.eastDoor = eastDoor;
    }

    public void setWestDoor(Door westDoor) {
        this.westDoor = westDoor;
    }

    public void setNorthDoor(Door northDoor) {
        this.northDoor = northDoor;
    }

    public void setSouthDoor(Door southDoor) {
        this.southDoor = southDoor;
    }

    public void setDescription(String text) { this.description = text; }

    private Door eastDoor;
    private Door westDoor;

    public Door getEastDoor() {
        return eastDoor;
    }

    public Door getWestDoor() {
        return westDoor;
    }

    public Door getNorthDoor() {
        return northDoor;
    }

    public Door getSouthDoor() {
        return southDoor;
    }

    public String getDescription() { return description; };

    private Door northDoor;
    private Door southDoor;

    public Room(int number) {
        this.number = number;
    }

    public Item getItemFromList(String desc) {
        for (Item item: items) {
            if (item.getDescription().equals(desc)) return item;
            for (Item innerItem: item.getInnerItems()) {
                if (innerItem.getDescription().equals(desc)) return innerItem;
                for (Item innerInnerItem: innerItem.getInnerItems()) {
                    if (innerInnerItem.getDescription().equals(desc)) return innerInnerItem;
                }
            }
        }
        return null;
    }

    public Item getItemByCode(String code) {
        for (Item item: items) {
            if (item.getCode().equals(code)) return item;
            for (Item innerItem: item.getInnerItems()) {
                if (innerItem.getCode().equals(code)) return innerItem;
                for (Item innerInnerItem: innerItem.getInnerItems()) {
                    if (innerInnerItem.getCode().equals(code)) return innerInnerItem;
                }
            }
        }
        return null;
    }

    public void removeItemFromList(Item itemToRemove) {
        if (items.remove(itemToRemove)) return;
        for (Item item: items) {
            if (item.getInnerItems().remove(itemToRemove)) return;
            for (Item innerItem: item.getInnerItems()) {
                if (innerItem.getInnerItems().remove(itemToRemove)) return;
            }
        }

    }

    public void addItemToList(Item item) {
        if (items == null) items = new ArrayList<>();
        items.add(item);
    }


    public int getNumber() {
        return number;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public int getSouth() {
        return south;
    }

    public int getNorth() {
        return north;
    }

    public int getEast() {
        return east;
    }

    public int getWest() {
        return west;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public void setSouth(int south, String itemToOpen) {
        this.south = south;
        southDoor = new Door(itemToOpen, "south door");

    }

    public void setNorth(int north, String itemToOpen) {
        this.north = north;
        northDoor = new Door(itemToOpen, "north door");
    }

    public void setEast(int east, String itemToOpen) {
        this.east = east;
        eastDoor = new Door(itemToOpen, "east door");
    }

    public void setWest(int west, String itemToOpen) {
        this.west = west;
        westDoor = new Door(itemToOpen, "west door");
    }

    public String toString() {
        if (items.size() == 0) return description;
        StringBuilder sb = new StringBuilder();
        sb.append(description);
        sb.append("\r\n");
        sb.append("There is ");
        for (Item item: items) {
            sb.append(item.getText());
            sb.append(", ");
        }

        return  sb.toString().substring(0, sb.length()-2);
    }


}
