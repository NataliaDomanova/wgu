package com.example.natalia.adventure_gamemobile_app_3;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;

import java.io.File;

public class MainMenuActivity extends Activity {

    Button btnNewGame;
    Button btnLoadGame;
    Button btnAbout;
    Button btnHelp;
    Button btnExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        initializeComponents();
    }

    private void initializeComponents() {
        btnNewGame = findViewById(R.id.btnNewGame);
        btnLoadGame = findViewById(R.id.btnLoadGame);
        btnAbout = findViewById(R.id.btnAbout);
        btnHelp = findViewById(R.id.btnHelp);
        btnExit = findViewById(R.id.btnExit);

        btnNewGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("GAME", "0");
                startActivity(intent);
            }
        });

        String loadFilePath = getApplicationContext().getFilesDir() + "/" + "inventory.xml";
        File loadFile = new File( loadFilePath );
        if (!loadFile.exists()) {
            btnLoadGame.setClickable(false);
        }

        btnLoadGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("GAME", "1");
                startActivity(intent);
            }
        });

        btnAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AboutActivity.class);
                startActivity(intent);
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), HelpActivity.class);
                startActivity(intent);
            }
        });
    }

}
