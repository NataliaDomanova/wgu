package com.example.natalia.adventure_gamemobile_app_3;

public class Door {

    private boolean isOpen;
    private String itemToOpen;
    private String description;

    public String getDescription() {
        return description;
    }

    public Door(String itemToOpen, String description) {
        this.isOpen = false;
        this.itemToOpen = itemToOpen;
        this.description = description;

    }

    public void setOpen() {
        this.isOpen = true;
    }

    public void setItemToOpen(String itemToOpen) {
        this.itemToOpen = itemToOpen;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public String getItemToOpen() {
        return itemToOpen;
    }

}
