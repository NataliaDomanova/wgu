package com.example.natalia.adventure_gamemobile_app_3;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;

public class CongratsActivity extends Activity {

    Button btnMainMenu;
    Button btnExitEnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_congrats);

        btnMainMenu = findViewById(R.id.btnMainMenu);
        btnExitEnd = findViewById(R.id.btnExitEnd);

        btnExitEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnMainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainMenuActivity.class);
                startActivity(intent);
            }
        });
    }

}
