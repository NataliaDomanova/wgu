package com.example.natalia.adventure_gamemobile_app_3;

import android.util.Log;

import java.util.ArrayList;

public  class Item {

    private String code;
    private String description;
    private String text;
    private ArrayList<Item> innerItems;
    private boolean isOpen;
    private String itemToOpen;
    private String closeText;
    private int numOfInnerItems;
    private boolean canOpen;
    private boolean canPickUp;
    private boolean canExplore;
    private boolean canRead;
    private String textToRead;

    public boolean canExplore() {
        return canExplore;
    }

    private boolean isVisible;

    public String getText() { return text; }


    public boolean canRead() {
        return canRead;
    }

    public String getTextToRead() {
        return textToRead;
    }

    public Item(String code, String description, boolean isOpen, String itemToOpen,
                String closeText, String title, int num, boolean isVisible, boolean canOpen,
                boolean canPickUp, boolean canExplore, boolean canRead, String textToRead) {
        this.code = code;
        this.description = description;
        this.isOpen = isOpen;
        this.itemToOpen = itemToOpen;
        this.closeText = closeText;
        this.numOfInnerItems = num;
        this.isVisible = isVisible;
        this.canOpen = canOpen;
        this.canPickUp = canPickUp;
        this.canExplore = canExplore;
        this.canRead = canRead;
        this.textToRead = textToRead;
        this.text = title;


        innerItems = new ArrayList<>();

    }

    public void addInnerItem(Item item) {
        this.innerItems.add(item);
    }

    public int getNumOfInnerItems() {
        return numOfInnerItems;
    }

    public String getOpenText() {
        if (innerItems.size() == 0) return "There is nothing";
        StringBuilder sb = new StringBuilder();
        sb.append("There is ");
        for (Item item: innerItems) {
            sb.append(item.getText());
            sb.append(", ");
        }
        return sb.toString().substring(0, sb.length()-2);
    }


    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

   public ArrayList<Item> getInnerItems() {
        return innerItems;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen() {
        this.isOpen = true;
    }

    public String getItemToOpen() {
        return itemToOpen;
    }

    public String getCloseText() {
        return closeText;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible() {
        this.isVisible = true;
    }

    public boolean canOpen() {
        return this.canOpen;
    }

    public boolean canPickUp() {
        return  this.canPickUp;
    }

}
