package com.example.natalia.adventure_gamemobile_app_3;

import android.app.AlertDialog;
import android.app.Dialog;
import android.arch.core.util.Function;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class MainActivity extends AppCompatActivity {

    final Context context = this;

    ImageView imgRoom;
    TextView txtDescriptionRoom;
    Button btnInventory;
    Button btnOpen;
    Button btnNorth;
    Button btnUse;
    Button btnWest;
    Button btnEast;
    Button btnPickUp;
    Button btnSouth;
    Button btnExplore;
    Button btnLookAround;
    Button btnSaveGame;

    Room currentRoom;
    Room[] rooms = new Room[17];
    ArrayList<Item> inventory = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle extras = getIntent().getExtras();
        String extra = extras.getString("GAME");
        Boolean isNewGame = true;
        if (extra.equals("1")) {
            isNewGame = false;
        }
        initializeComponents();

        for (int i = 1; i < 17; i++) {
            if (i != 3) {

                    if (!isNewGame) {


                        try {
                            FileInputStream fis = context.openFileInput("savedroom"+i+".xml");
                            InputStreamReader isr = new InputStreamReader(fis);
                            BufferedReader bufferedReader = new BufferedReader(isr);
                            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                            factory.setNamespaceAware(true);
                            XmlPullParser parser = factory.newPullParser();
                            parser.setInput(bufferedReader);
                            initializeRoom(i, parser);
                        } catch (XmlPullParserException e) {
                            e.printStackTrace();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }


                    } else {
                        Resources resources = this.getResources();
                        int id = resources.getIdentifier("room" + i,
                                "xml", getPackageName());
                        XmlResourceParser parser = getResources().getXml(id);
                        initializeRoom(i, parser);
                    }
            }
        }
        if (!isNewGame) {
            readInventoryXml();
        } else {
            currentRoom = rooms[1];
        }

        changeComponents();
    }

    private void readInventoryXml()  {
        try {
            FileInputStream fis = context.openFileInput("inventory.xml");
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();

            parser.setInput(bufferedReader);

            parser.next();
            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    String elemName = parser.getName();
                    if (elemName.equals("position")) {
                        while (eventType != XmlPullParser.TEXT) {
                            eventType = parser.next();
                        }
                        currentRoom = rooms[Integer.parseInt(parser.getText())];
                    }
                    if (elemName.equals("item")) {
                        Item item = readItemXML(parser);
                        inventory.add(item);
                        System.out.println("Added item "+item.getCode());
                    }

                }
                eventType = parser.next();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void initializeRoom(int number, XmlPullParser parser) {
        Log.w("ROOMS: ", "I created a room"+number);
            Room room1 = new Room(number);
            rooms[number] = room1;

            try {
                parser.next();
                int eventType = parser.getEventType();
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    if (eventType == XmlPullParser.START_TAG) {
                        String elemName = parser.getName();

                        if (elemName.equals("text")) {
                            while (eventType != XmlPullParser.TEXT) {
                                eventType = parser.next();
                            }
                            //txtDescriptionRoom.setText(parser.getText());
                            rooms[number].setDescription(parser.getText());
                        }
                        if (elemName.equals("doors")) {
                            for (int i = 0; i < 4; ) {
                                eventType = parser.next();
                                if (eventType == XmlPullParser.START_TAG) {
                                    elemName = parser.getName();
                                    if (elemName.equals("east"))
                                        room1.setEast(Integer.valueOf(parser.getAttributeValue(null, "room")),
                                                parser.getAttributeValue(null, "itemToOpen"));
                                    if (elemName.equals("west"))
                                        room1.setWest(Integer.valueOf(parser.getAttributeValue(null, "room")),
                                                parser.getAttributeValue(null, "itemToOpen"));
                                    if (elemName.equals("north"))
                                        room1.setNorth(Integer.valueOf(parser.getAttributeValue(null, "room")),
                                                parser.getAttributeValue(null, "itemToOpen"));
                                    if (elemName.equals("south"))
                                        room1.setSouth(Integer.valueOf(parser.getAttributeValue(null, "room")),
                                                parser.getAttributeValue(null, "itemToOpen"));
                                    i++;
                                }
                            }

                        }
                        if (elemName.equals("items")) {
                            eventType = parser.next();
                            while (eventType != XmlPullParser.START_TAG) {
                                eventType = parser.next();
                            }
                            elemName = parser.getName();

                            while (elemName.equals("item")) {
                                Item item = readItemXML(parser);
                                room1.addItemToList(item);

                                for (int i = 0; i < item.getNumOfInnerItems(); ) {
                                    eventType = parser.next();
                                    if (eventType == XmlPullParser.START_TAG) {
                                        elemName = parser.getName();
                                        if (elemName.equals("item")) {
                                            Item item1 = readItemXML(parser);
                                            item.addInnerItem(item1);

                                            for (int j = 0; j < item1.getNumOfInnerItems(); ) {
                                                eventType = parser.next();
                                                if (eventType == XmlPullParser.START_TAG) {
                                                    elemName = parser.getName();
                                                    if (elemName.equals("item")) {
                                                        item1.addInnerItem(readItemXML(parser));
                                                        j++;
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                                eventType = parser.next();

                                while (eventType != XmlPullParser.START_TAG) {
                                    eventType = parser.next();
                                    if (eventType == XmlPullParser.END_DOCUMENT) {
                                        return;
                                    }
                                }
                                elemName = parser.getName();
                            }
                        }
                    }
                    eventType = parser.next();
                }

            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    private Item readItemXML(XmlPullParser parser) throws XmlPullParserException, IOException {
        int eventType = parser.getEventType();
        String code = parser.getAttributeValue(null, "code");
        String description = parser.getAttributeValue(null, "description");
        boolean isOpen = Boolean.parseBoolean(parser.getAttributeValue(null, "open"));
        boolean isVisible = Boolean.parseBoolean(parser.getAttributeValue(null, "visible"));
        boolean canOpen = Boolean.parseBoolean(parser.getAttributeValue(null, "canOpen"));
        boolean canPickUp = Boolean.parseBoolean(parser.getAttributeValue(null, "canPickUp"));
        boolean canExpplore = Boolean.parseBoolean(parser.getAttributeValue(null, "canExplore"));
        boolean canRead = Boolean.parseBoolean(parser.getAttributeValue(null, "canRead"));
        String textToRead = parser.getAttributeValue(null, "read");
        String itemToOpen = parser.getAttributeValue(null, "itemToOpen");
        int numOfInnerItems = Integer.valueOf(parser.getAttributeValue(null, "numOfItems"));
        String closeText = parser.getAttributeValue(null, "closeText");
        while (eventType != XmlPullParser.TEXT) {
            eventType = parser.next();
        }
        String title = parser.getText();

//        Log.w("ITEM: ", code+" desc: "+description+" title: "+title+ " isOpen: "+isOpen+" isVisible: "+isVisible+" canOpen: "+canOpen+
//            " canPickUp: "+canPickUp+" canExplore: "+canExpplore+" canRead: "+canRead+" TextToRead: "+
//        textToRead+" itemToOpen: "+itemToOpen+" numOfInnerItems: "+numOfInnerItems);

        return new Item(code, description, isOpen, itemToOpen, closeText, title,
                numOfInnerItems, isVisible, canOpen, canPickUp, canExpplore, canRead, textToRead);
    }

    public void checkDoor(final int side, final Button btnSide, final Door sideDoor) {

        if (side == 0) {
            btnSide.setClickable(false);
            btnSide.setVisibility(View.INVISIBLE);
        }

        else {
            btnSide.setVisibility(View.VISIBLE);
            btnSide.setClickable(true);


                btnSide.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (sideDoor.isOpen() || sideDoor.getItemToOpen().equals("open")) {
                            if (side == -1) {
                                Intent intent = new Intent(getApplicationContext(), CongratsActivity.class);
                                startActivity(intent);
                            } else {
                                currentRoom = rooms[side];
                                changeComponents();
                            }
                        } else {
                            if (sideDoor.getItemToOpen().contains("code")) {

                                final Dialog dialog = new Dialog(context);
                                dialog.setContentView(R.layout.dialog_enter_code);
                                final TextView txtCode = dialog.findViewById(R.id.txtCode);
                                Button btnEnterCode = dialog.findViewById(R.id.btnEnterCode);

                                btnEnterCode.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (!txtCode.getText().toString().isEmpty() && txtCode.getText().toString()
                                                .equals(currentRoom.getEastDoor().getItemToOpen().substring(5))) {
                                            sideDoor.setOpen();
                                            txtDescriptionRoom.setText("The door is open!");
                                        } else {
                                            txtDescriptionRoom.setText("Hmm, sth is wrong...");
                                        }
                                        dialog.dismiss();
                                    }
                                });

                                dialog.show();

                            } else {
                                if (sideDoor.getItemToOpen().contains("remote")) {
                                    //get the item and num of room
                                    String itemToOpen = sideDoor.getItemToOpen().substring(7);
                                    StringBuilder sb = new StringBuilder();
                                    for (Character c : itemToOpen.toCharArray()) {
                                        if (Character.isDigit(c)) sb.append(c);
                                        if (c == '.') break;
                                    }
                                    int room = Integer.parseInt(sb.toString());
                                    if (rooms[room].getItemByCode(itemToOpen).isOpen()) {
                                        sideDoor.setOpen();
                                        currentRoom = rooms[side];
                                        changeComponents();
                                    } else {
                                        if (sideDoor.getItemToOpen().contains("candle")) {
                                            txtDescriptionRoom.setText("I can't see anything...");
                                        } else txtDescriptionRoom.setText("This door is closed");
                                    }
                                } else if (sideDoor.getItemToOpen().contains("handle")) {
                                    txtDescriptionRoom.setText("There is no handle! What am I going to do?");
                                } else txtDescriptionRoom.setText("This door is closed");

                            }
                        }
                    }
                });

        }
    }

    private void changeComponents() {

        txtDescriptionRoom.setText(currentRoom.toString());

        Resources resources = getResources();
        int id = resources.getIdentifier("room" + currentRoom.getNumber(),
                "drawable", "com.example.natalia.adventure_gamemobile_app_3");
        imgRoom.setImageResource(id);

        if (currentRoom.toString().contains("candle")) {
            id = resources.getIdentifier("roomblack", "drawable",
                    "com.example.natalia.adventure_gamemobile_app_3");
            imgRoom.setImageResource(id);
        }



            checkDoor(currentRoom.getEast(), btnEast, currentRoom.getEastDoor());
            checkDoor(currentRoom.getWest(), btnWest, currentRoom.getWestDoor());
            checkDoor(currentRoom.getNorth(), btnNorth, currentRoom.getNorthDoor());
            checkDoor(currentRoom.getSouth(), btnSouth, currentRoom.getSouthDoor());

            btnOpen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu menu = new PopupMenu(MainActivity.this, v);
                    for (Item item : currentRoom.getItems()) {
                        if (item.isVisible() && item.canOpen())
                            menu.getMenu().add(item.getDescription());
                        for (Item innerItem : item.getInnerItems()) {

                            if (innerItem.isVisible() && innerItem.canOpen())
                                menu.getMenu().add(innerItem.getDescription());

                            for (Item innerInnerItem : innerItem.getInnerItems()) {
                                if (innerInnerItem.isVisible() && innerInnerItem.canOpen())
                                    menu.getMenu().add(innerInnerItem.getDescription());
                            }
                        }

                    }
                    menu.show();
                    menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            Item item = currentRoom.getItemFromList(menuItem.toString());
                            if (item.isOpen()) {
                                txtDescriptionRoom.setText(item.getOpenText());
                                for (Item innerItem : item.getInnerItems()) {
                                    innerItem.setVisible();
                                }
                            } else txtDescriptionRoom.setText(item.getCloseText());
                            return true;
                        }
                    });
                }
            });

            btnExplore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu menu = new PopupMenu(MainActivity.this, v);
                    for (Item item : currentRoom.getItems()) {

                        if (item.isVisible() && (item.canExplore() || (item.isOpen() && item.canRead())))
                            menu.getMenu().add(item.getDescription());
                        for (Item innerItem : item.getInnerItems()) {

                            if (innerItem.isVisible() && (innerItem.canExplore() || (item.isOpen() && innerItem.canRead())))
                                menu.getMenu().add(innerItem.getDescription());

                            for (Item innerInnerItem : innerItem.getInnerItems()) {
                                if (innerInnerItem.isVisible() && (innerInnerItem.canExplore() || (item.isOpen() && innerInnerItem.canRead())))
                                    menu.getMenu().add(innerInnerItem.getDescription());
                            }
                        }

                    }
                    menu.show();
                    menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            Item item = currentRoom.getItemFromList(menuItem.toString());
                            if (item.canRead() && item.isOpen()) {
                                txtDescriptionRoom.setText(item.getTextToRead());
                                return true;
                            }
                            if (item.canRead() && !item.isOpen()) {
                                txtDescriptionRoom.setText("I can't read it...");
                                return true;
                            }
                            if (item.isOpen()) {
                                txtDescriptionRoom.setText(item.getOpenText());
                                for (Item innerItem : item.getInnerItems()) {
                                    innerItem.setVisible();
                                }
                            } else txtDescriptionRoom.setText(item.getCloseText());
                            return true;
                        }
                    });
                }
            });

            btnPickUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu menu = new PopupMenu(MainActivity.this, v);
                    for (Item item : currentRoom.getItems()) {
                        if (item.isVisible() && item.canPickUp())
                            menu.getMenu().add(item.getDescription());
                        for (Item innerItem : item.getInnerItems()) {

                            if (innerItem.isVisible() && innerItem.canPickUp())
                                menu.getMenu().add(innerItem.getDescription());

                            for (Item innerInnerItem : innerItem.getInnerItems()) {
                                if (innerInnerItem.isVisible() && innerInnerItem.canPickUp())
                                    menu.getMenu().add(innerInnerItem.getDescription());
                            }
                        }

                    }
                    menu.show();
                    menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            Item item = currentRoom.getItemFromList(menuItem.toString());
                            inventory.add(item);
                            currentRoom.removeItemFromList(item);
                            return true;
                        }
                    });


                }
            });

            btnUse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu menu = new PopupMenu(MainActivity.this, v);
                    for (Item item : inventory) {
                        menu.getMenu().add(item.getDescription());
                    }
                    menu.show();
                    menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(final MenuItem menuItem) {
                            PopupMenu menu = new PopupMenu(MainActivity.this, btnUse);
                            for (Item item : currentRoom.getItems()) {
                                if (item.isVisible() && !item.isOpen())
                                    menu.getMenu().add(item.getDescription());
                                for (Item innerItem : item.getInnerItems()) {

                                    if (innerItem.isVisible() && !innerItem.isOpen())
                                        menu.getMenu().add(innerItem.getDescription());

                                    for (Item innerInnerItem : innerItem.getInnerItems()) {
                                        if (innerInnerItem.isVisible() && !innerInnerItem.isOpen())
                                            menu.getMenu().add(innerInnerItem.getDescription());
                                    }
                                }
                            }
                            if (currentRoom.getSouth() != 0)
                                menu.getMenu().add(currentRoom.getSouthDoor().getDescription());
                            if (currentRoom.getNorth() != 0)
                                menu.getMenu().add(currentRoom.getNorthDoor().getDescription());
                            if (currentRoom.getEast() != 0)
                                menu.getMenu().add(currentRoom.getEastDoor().getDescription());
                            if (currentRoom.getWest() != 0)
                                menu.getMenu().add(currentRoom.getWestDoor().getDescription());
                            menu.show();
                            menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem menuItem1) {
                                    Item useItem = getItemFromInventory(menuItem.toString());
                                    Item openItem = currentRoom.getItemFromList(menuItem1.toString());
                                    if (openItem == null) {
                                        Door openDoor = null;
                                        switch (menuItem1.toString()) {
                                            case "south door":
                                                openDoor = currentRoom.getSouthDoor();
                                                break;
                                            case "north door":
                                                openDoor = currentRoom.getNorthDoor();
                                                break;
                                            case "west door":
                                                openDoor = currentRoom.getWestDoor();
                                                break;
                                            case "east door":
                                                openDoor = currentRoom.getEastDoor();
                                                break;
                                            default:
                                                break;
                                        }
                                        if (openDoor.getItemToOpen().equals(useItem.getCode())) {
                                            openDoor.setOpen();
                                            txtDescriptionRoom.setText("The door is open!");
                                            inventory.remove(useItem);
                                        } else {
                                            txtDescriptionRoom.setText("Hmm, it doesn't work");
                                        }
                                    } else {
                                        if (useItem.getCode().equals(openItem.getItemToOpen())) {
                                            openItem.setOpen();
                                            if (!useItem.getCode().contains("matches")) {
                                                inventory.remove(useItem);
                                            }
                                            for (Item innerItem : openItem.getInnerItems()) {
                                                innerItem.setVisible();
                                                txtDescriptionRoom.setText(openItem.getOpenText());
                                            }
                                            if (useItem.getCode().contains("matches")) {
                                                Resources resources = getResources();
                                                int id = resources.getIdentifier("room" + currentRoom.getNumber(),
                                                        "drawable", "com.example.natalia.adventure_gamemobile_app_3");
                                                imgRoom.setImageResource(id);
                                            }
                                            if (openItem.canRead()) {
                                                txtDescriptionRoom.setText(openItem.getTextToRead());
                                            }

                                        } else {
                                            txtDescriptionRoom.setText("Hmm, it doesn't work");
                                        }
                                    }
                                    return true;
                                }
                            });
                            return true;
                        }
                    });
                }
            });

    }

    private Item getItemFromInventory(String desc) {
        for(Item item: inventory) {
            if (item.getDescription().equals(desc)) return item;
        }
        return null;
    }

    private void writeXmlInventory() {
        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = null;
        try {
            documentBuilder = documentFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        Document document = documentBuilder.newDocument();

        Element root = document.createElement("inventory");
        Element position = document.createElement("position");
        position.appendChild(document.createTextNode(currentRoom.getNumber()+""));
        root.appendChild(position);
        document.appendChild(root);

        for (Item item: inventory) {
            root.appendChild(createItemElement(document, item));
        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        try {
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(context.getFilesDir(), "inventory.xml"));

            transformer.transform(domSource, streamResult);

            System.out.println("Done creating XML File");
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }



    private void writeXmlRoom(Room room)  {
        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = null;
        try {
            documentBuilder = documentFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        Document document = documentBuilder.newDocument();

        Element root = document.createElement("description");
        document.appendChild(root);

        Element text = document.createElement("text");

        text.appendChild(document.createTextNode(room.getDescription()));
        root.appendChild(text);

        Element doors = document.createElement("doors");
        root.appendChild(doors);

        doors.appendChild(createDoorElement(document, room.getEast(), room.getEastDoor(), "east"));
        doors.appendChild(createDoorElement(document, room.getWest(), room.getWestDoor(), "west"));
        doors.appendChild(createDoorElement(document, room.getNorth(), room.getNorthDoor(),  "north"));
        doors.appendChild(createDoorElement(document, room.getSouth(), room.getSouthDoor(), "south"));

        Element items = document.createElement("items");
        root.appendChild(items);

        for (Item item : room.getItems()) {
                items.appendChild(createItemElement(document, item));
        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        try {
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(context.getFilesDir(), "savedroom"+room.getNumber()+".xml"));

            transformer.transform(domSource, streamResult);

            System.out.println("Done creating XML File");
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }


    }

    private Element createItemElement(Document document, Item item) {
        Element itemElem = document.createElement("item");

        setAttributeToItem(itemElem, document, "code", item.getCode());
        setAttributeToItem(itemElem, document, "description", item.getDescription());
        setAttributeToItem(itemElem, document, "itemToOpen", item.getItemToOpen());
        setAttributeToItem(itemElem, document, "open", item.isOpen());
        setAttributeToItem(itemElem, document, "closeText", item.getCloseText());
        setAttributeToItem(itemElem, document, "canOpen", item.canOpen());
        setAttributeToItem(itemElem, document, "canPickUp", item.canPickUp());
        setAttributeToItem(itemElem, document, "canExplore", item.canExplore());
        setAttributeToItem(itemElem, document, "canRead", item.canRead());
        setAttributeToItem(itemElem, document, "read", item.getTextToRead());
        setAttributeToItem(itemElem, document, "visible", item.isVisible());
        itemElem.appendChild(document.createTextNode(item.getText()));
        int numOfInnerItems = item.getInnerItems().size();
        setAttributeToItem(itemElem, document, "numOfItems", numOfInnerItems);
        if (numOfInnerItems != 0) {
            Element innerItems = document.createElement("items");
            for (Item inner : item.getInnerItems()) {
                innerItems.appendChild(createItemElement(document, inner));
            }
            itemElem.appendChild(innerItems);
        }

        return  itemElem;
    }

    private <T> void setAttributeToItem(Element element, Document document, String attrName, T value) {
        Attr attr = document.createAttribute(attrName);
        attr.setValue(value+"");
        element.setAttributeNode(attr);
    }

    private Element createDoorElement(Document document, int roomSide, Door doorSide, String side) {
        Element door = document.createElement(side);
        Attr exitRoom = document.createAttribute("room");
        exitRoom.setValue(roomSide+"");
        Attr eastItemToOpen = document.createAttribute("itemToOpen");
        eastItemToOpen.setValue(doorSide.getItemToOpen());
        door.setAttributeNode(exitRoom);
        door.setAttributeNode(eastItemToOpen);
        door.appendChild(document.createTextNode("Room "+roomSide));

        return door;
    }

    private void initializeComponents() {
        imgRoom = findViewById(R.id.imgRoom);
        txtDescriptionRoom = findViewById(R.id.txtDwscriptionRoom);
        btnInventory = findViewById(R.id.btnInventory);
        btnOpen = findViewById(R.id.btnOpen);
        btnNorth = findViewById(R.id.btnNorth);
        btnUse = findViewById(R.id.btnUse);
        btnWest = findViewById(R.id.btnWest);
        btnEast = findViewById(R.id.btnEast);
        btnPickUp = findViewById(R.id.btnPickUp);
        btnSouth = findViewById(R.id.btnSouth);
        btnExplore = findViewById(R.id.btnExplore);
        btnLookAround = findViewById(R.id.btnLookAround);
        btnSaveGame = findViewById(R.id.btnSaveGame);
        btnSaveGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               for (int i = 1; i < rooms.length; i++) {
                   if (i != 3) {
                       writeXmlRoom(rooms[i]);

                   }
               }

               writeXmlInventory();
            }
        });
        btnLookAround.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtDescriptionRoom.setText(currentRoom.toString());
            }
        });

        btnInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_inventory);

                ListView listInventory = dialog.findViewById(R.id.listInventory);
                ArrayList<String> stringItems = new ArrayList<>();
                for (Item item: inventory) {
                    stringItems.add(item.getDescription());
                }
                ArrayAdapter adapter = new ArrayAdapter<>(dialog.getContext(), android.R.layout.simple_list_item_1, stringItems);
                listInventory.setAdapter(adapter);
                Button btnExitInventory = dialog.findViewById(R.id.btnExitInventory);

                btnExitInventory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();

            }
        });
    }

}

