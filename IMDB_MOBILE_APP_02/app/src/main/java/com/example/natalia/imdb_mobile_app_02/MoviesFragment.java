package com.example.natalia.imdb_mobile_app_02;


import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class MoviesFragment extends Fragment {

    TextView txtSearchMovie;
    Button btnSearchMovie;
    Button btnAddMovie;

    ListView listMovies;
    ArrayList<String> moviesList;
    ArrayAdapter adapter;

    public MoviesFragment() {
        // Required empty public constructor
    }


    public void initComponents(View view) {
        txtSearchMovie = (TextView) view.findViewById(R.id.txtSearchMovie);
        btnSearchMovie = (Button) view.findViewById(R.id.btnSearchMovie);
        listMovies = (ListView) view.findViewById(R.id.listMovies);

        btnSearchMovie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshListmovies(txtSearchMovie.getText().toString());

            }
        });

        listMovies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position,
                                    long id) {
                //open new activity with it's info
                String fullName = (String) parent.getItemAtPosition(position);
                String title = fullName.split(",")[0];
                String year = fullName.split(",")[1];
                Bundle bundle = new Bundle();
                bundle.putString("title", title);
                bundle.putString("year", year);
                SingleMovieFragment fragment = new SingleMovieFragment();
                fragment.setArguments(bundle);
                setFragment(fragment);
            }
        });

        btnAddMovie = (Button) view.findViewById(R.id.btnAddMovie);
        btnAddMovie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddMovieFragment fragment = new AddMovieFragment();
                setFragment(fragment);
            }
        });
    }

    public void refreshListmovies(String line) {
        moviesList = MainActivity.openDatabase.searchMovie(line);
        adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, moviesList);
        listMovies.setAdapter(adapter);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_movies, container, false);

        initComponents(view);

        refreshListmovies(null);
        return view;

    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrame, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }
}
