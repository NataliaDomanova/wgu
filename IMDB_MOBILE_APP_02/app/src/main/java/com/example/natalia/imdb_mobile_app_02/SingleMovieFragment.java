package com.example.natalia.imdb_mobile_app_02;


import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class SingleMovieFragment extends Fragment {

    ImageView imgPoster;
    TextView txtInfo;
    ListView listActors;
    ArrayList<String> actors;
    ArrayAdapter adapter;


    public SingleMovieFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_single_movie, container, false);
        String title = this.getArguments().getString("title");
        String year = this.getArguments().getString("year");
        initComponents(view, title, year);
        return view;
    }

    public void initComponents(View view, String title, String year) {
        imgPoster = (ImageView) view.findViewById(R.id.imgPoster);

        Resources resources = view.getResources();
        int id = resources.getIdentifier((title+year).toLowerCase().replace(" ", ""),
                "drawable", "com.example.natalia.imdb_mobile_app_02");
        imgPoster.setImageResource(id);

        txtInfo = (TextView) view.findViewById(R.id.txtInfo);
        txtInfo.setText(MainActivity.openDatabase.searchParticularMovie(title, year));


        listActors = (ListView) view.findViewById(R.id.listActors);

        actors = MainActivity.openDatabase.searchActorsInMovie(title, year);
        adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, actors);
        listActors.setAdapter(adapter);


    }

}
