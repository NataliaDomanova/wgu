package com.example.natalia.imdb_mobile_app_02;


import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class SingleActorFragment extends Fragment {

    ImageView imgPhoto;
    TextView txtInfo;
    ListView listFilmography;
    ArrayList<String> filmography;
    ArrayAdapter adapter;


    public SingleActorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_single_actor, container, false);
        String firstName = this.getArguments().getString("firstName");
        String lastName = this.getArguments().getString("lastName");
        initComponents(view, firstName, lastName);
        return view;
    }

    public void initComponents(View view, String firstName, String lastName) {
        imgPhoto = (ImageView) view.findViewById(R.id.imgPhoto);

        Resources resources = view.getResources();
        int id = resources.getIdentifier((firstName+lastName).toLowerCase(),
                "drawable", "com.example.natalia.imdb_mobile_app_02");
        imgPhoto.setImageResource(id);


        txtInfo = (TextView) view.findViewById(R.id.txtInfo);
        txtInfo.setText(MainActivity.openDatabase.searchPArticularActor(firstName, lastName));


        listFilmography = (ListView) view.findViewById(R.id.listFilmography);

        filmography = MainActivity.openDatabase.searchFilmography(firstName, lastName);
        adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, filmography);
        listFilmography.setAdapter(adapter);



    }

}
