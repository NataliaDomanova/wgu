package com.example.natalia.imdb_mobile_app_02;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

public class OpenDatabase extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "imdb.db";
    private static final int DATABASE_VERSION = 1;



    OpenDatabase(Context context)
    {
        super( context, DATABASE_NAME, null, DATABASE_VERSION );
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public String searchPArticularActor(String firstName, String lastName) {
        String[] args = new String[] { firstName, lastName };
        StringBuilder record = new StringBuilder();
        record.append(firstName);
        record.append(" ");
        record.append(lastName);
        record.append(System.getProperty("line.separator"));

        Cursor c = MainActivity.database.rawQuery("SELECT * FROM actor where first_name like ? and last_name like ?", args);
        if (c != null)
        {
            if (c.moveToFirst())
            {
                do
                {
                    if (c.getString(1).equals(firstName)) {
                        record.append("Date of Birth: ");
                        record.append(c.getString(3));
                        record.append(System.getProperty("line.separator"));
                        if (c.getString(4) != null) {
                            record.append("Date of death: ");
                            record.append(c.getString(4));
                        }
                        break;
                    }
                } while (c.moveToNext());
            }
            c.close();
        }

        return record.toString();
    }

    public String searchParticularMovie(String title, String year) {
        String[] args = new String[] {title, year};
        StringBuilder record = new StringBuilder();
        record.append(title);
        record.append("\r\nYear: ");
        record.append(year);

        Cursor c = MainActivity.database.rawQuery("select * from film where title = ? and year = ?", args);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    record.append("\r\nBudget: ");
                    if (c.getString(3) != null) record.append(c.getString(3));
                    record.append("$\r\nProfit: ");
                    if (c.getString(4) != null) record.append(c.getString(4));
                    String genre = this.getGenreById(Integer.parseInt(c.getString(5)));
                    record.append("$\r\nGenre: ");
                    record.append(genre);
                } while (c.moveToNext());
            }
            c.close();
        }

        return record.toString();
    }



    public ArrayList<String> searchFilmography(String firstName, String lastName) {
        String[] args = new String[]
                {lastName};
        Cursor c = MainActivity.database.rawQuery(
                "select film.id_film, film.title, film.year from film, filmography, " +
                        "actor where film.id_film = filmography.id_film and " +
                        "actor.id_actor = filmography.id_actor and actor.last_name=?", args);

        ArrayList<String> records = new ArrayList<>();
        if (c != null)
        {
            if (c.moveToFirst())
            {
                do
                {
                    StringBuilder sb = new StringBuilder();
                    String id = c.getString(0);
                    String title = c.getString(1);
                    sb.append(title);
                    sb.append(" ");
                    String year = c.getString(2);
                    sb.append(year);
                    Log.w("ACTORS", "ID = " + id + " NAME =" +
                            title+" "+year);

                    records.add(sb.toString());
                } while (c.moveToNext());
            }
            c.close();
        }

        if (records.size() == 0) records.add("No matches :(");
        for (String record: records) {
            Log.w("MOVIE", record);
        }

        return records;
    }



    public ArrayList<String> searchActor(String line) {
        String[] args = null;
        Cursor c = MainActivity.database.rawQuery("SELECT * FROM actor", args);
        if (line != null) {
            args = new String[]{'%' + line + '%', '%' + line + '%'};
            c = MainActivity.database.rawQuery("SELECT * FROM actor where last_name like ? or first_name like ?", args);
        }
        ArrayList<String> records = new ArrayList<>();
        if (c != null)
        {
            if (c.moveToFirst())
            {
                do
                {
                    StringBuilder sb = new StringBuilder();
                    String id = c.getString(0);
                    String firstName = c.getString(1);
                    sb.append(firstName);
                    sb.append(" ");
                    String lastName = c.getString(2);
                    sb.append(lastName);
                    Log.w("ACTORS", "ID = " + id + " NAME =" +
                            firstName+" "+lastName);

                    records.add(sb.toString());
                } while (c.moveToNext());
            }
            c.close();
        }

        if (records.size() == 0) records.add("No matches :(");
        for (String record: records) {
            Log.w("ACTOR", record);
        }

        return records;

    }

    public ArrayList<String> searchMovie(String line) {
        String[] args = null;
        Cursor c = MainActivity.database.rawQuery("SELECT * FROM film", args);
        if (line != null) {
            args = new String[]{'%' + line + '%', '%' + line + '%'};
            c = MainActivity.database.rawQuery("SELECT * FROM film where title like ? or year like ?", args);
        }
        ArrayList<String> records = new ArrayList<>();
        if (c != null)
        {
            if (c.moveToFirst())
            {
                do
                {
                    StringBuilder sb = new StringBuilder();
                    String id = c.getString(0);
                    String title = c.getString(1);
                    sb.append(title);
                    sb.append(",");
                    String year = c.getString(2);
                    sb.append(year);

                    records.add(sb.toString());
                } while (c.moveToNext());
            }
            c.close();
        }

        if (records.size() == 0) records.add("No matches :(");
        for (String record: records) {
            Log.w("MOVIE", record);
        }

        return records;
    }

    public ArrayList<String> searchEverything(String s) {
        ArrayList<String> records = new ArrayList<>();
        ArrayList<String> actors = this.searchActor(s);
        ArrayList<String> movies = this.searchMovie(s);
        if (actors.contains("No matches :(") && movies.contains("No matches :(")) records.add("No matches :(");
            else {
            if (actors.contains("No matches :(") && !movies.contains("No matches :(")) records = movies;
            else {
                if (!actors.contains("No matches :(") && movies.contains("No matches :(")) records = actors;
                else {
                    records.addAll(actors);
                    records.addAll(movies);
                }
            }
        }
        Collections.sort(records);
        return records;
    }

    public ArrayList<String> searchActorsInMovie(String title, String year) {
        String[] args = new String[] {title, year};
        Cursor c = MainActivity.database.rawQuery(
                "select actor.id_actor, actor.first_name, actor.last_name from " +
                        "actor, filmography, film where film.id_film = filmography.id_film and " +
                        "actor.id_actor = filmography.id_actor and title=? and year=?", args);

        ArrayList<String> records = new ArrayList<>();
        if (c != null)
        {
            if (c.moveToFirst())
            {
                do
                {
                    StringBuilder sb = new StringBuilder();
                    String id = c.getString(0);
                    String firstName = c.getString(1);
                    sb.append(firstName);
                    sb.append(" ");
                    String lastName = c.getString(2);
                    sb.append(lastName);
                    Log.w("ACTORS", "ID = " + id + " NAME =" +
                            title+" "+year);

                    records.add(sb.toString());
                } while (c.moveToNext());
            }
            c.close();
        }

        if (records.size() == 0) records.add("No matches :(");
        for (String record: records) {
            Log.w("MOVIE", record);
        }

        return records;

    }

    public ArrayList<String> getGenres() {
        Cursor c = MainActivity.database.rawQuery("select description from genre", null);
        ArrayList<String> records = new ArrayList<>();
        if (c!= null) {
            if (c.moveToFirst()) {
                do
                {
                    records.add(c.getString(0));
                    Log.w("GENRE", c.getString(0));
                } while (c.moveToNext());
            }
            c.close();
        }

        return records;
    }

    public int getGenreByName(String genre) {
        String[] args = new String[] { genre };
        int id = 0;
        Cursor c = MainActivity.database.rawQuery("select id_genre from genre where description=?", args);
        if (c!= null) {
            if (c.moveToFirst()) {
                do {
                    id = Integer.parseInt(c.getString(0));
                } while (c.moveToNext());
            }
            c.close();
        }

        return id;
    }

    public String getGenreById(int id) {
        String[] args = new String[] {id+""};
        String genre = "";
        Cursor c = MainActivity.database.rawQuery("select description from genre where id_genre = ?", args);
        if (c!= null) {
            if (c.moveToFirst()) {
                do {
                    genre = c.getString(0);
                } while (c.moveToNext());
            }
            c.close();
        }

        return genre;
    }

    public int getMovie(String title, int year) {
        String[] args = new String[] {title, year+""};
        int id = 0;
        Cursor c = MainActivity.database.rawQuery("select id_film from film where title= ? and year = ?", args);
        if (c!= null) {
            if (c.moveToFirst()) {
                do {
                    id = Integer.parseInt(c.getString(0));
                } while (c.moveToNext());
            }
            c.close();
        }

        return id;
    }

    public int getActorByName(String firstName, String lastName){
        String[] args =new String[] {lastName, firstName};
        int id = 0;
        Cursor c = MainActivity.database.rawQuery("select id_actor from actor where last_name like ? and first_name like ?", args);
        if (c!= null) {
            if (c.moveToFirst()) {
                do {
                    id = Integer.parseInt(c.getString(0));
                } while (c.moveToNext());
            }
            c.close();
        }

        return id;
    }

    public void addActor(String firstName, String lastName, String dateOfBirth, String dateOfDeath) {
        String query = "insert into Actor(first_name, last_name, date_of_birth, date_of_death) values" +
                "(\'"+firstName+"\', \'"+ lastName +"\', \'"+dateOfBirth+"\', \'"+dateOfDeath+"\')";
        MainActivity.database.execSQL(query);
    }

    public void addMovie(String title, int year, int budget, int profit, String genre, ArrayList<String> actors) {
        int idGenre = this.getGenreByName(genre);
        String query = "INSERT INTO Film(title, year, budget, profit, id_genre) VALUES(\'"+title+"\',"+
                year+", "+budget+","+profit+", "+idGenre+")";
        MainActivity.database.execSQL(query);
        //get movie id
        int idMovie = this.getMovie(title, year);

        for (String actor: actors) {
            this.addFilmographyRecord(idMovie, this.getActorByName(actor.split(" ")[0], actor.split(" ")[1]));
        }

    }

    public void addFilmographyRecord(int idFilm, int idActor) {
        MainActivity.database.execSQL("insert into Filmography(id_film, id_actor) values (\'"+idFilm+"\', \'"+idActor+"\')");
    }
}

