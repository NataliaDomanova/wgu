package com.example.natalia.imdb_mobile_app_02;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavView;
    private FrameLayout mainFrame;

    private SearchFragment searchFragment;
    private ActorsFragment actorsFragment;
    private MoviesFragment moviesFragment;

    private static final String DATABASE_PATH =
            "/data/data/com.example.natalia.imdb_mobile_app_02/databases/";
    private static final String DATABASE_PATH2 =
            "/data/data/com.example.natalia.imdb_mobile_app_02/databases";
    private static final String DATABASE_NAME = "imdb.db";
    private static final String LOG_TAG = "IMDB_DB";

    Context ctx;

    static OpenDatabase openDatabase;
    static SQLiteDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpDataBase();
        InitDataBase();

        bottomNavView =  (BottomNavigationView) findViewById(R.id.bottomNavView);
        mainFrame = (FrameLayout) findViewById(R.id.mainFrame);

        searchFragment = new SearchFragment();
        actorsFragment = new ActorsFragment();
        moviesFragment = new MoviesFragment();

        bottomNavView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch(menuItem.getItemId()) {
                    case R.id.nav_search:
                        setFragment(searchFragment);
                        return true;
                    case R.id.nav_actors:
                        setFragment(actorsFragment);
                        return true;
                    case R.id.nav_movies:
                        setFragment(moviesFragment);
                        return true;
                }

                return false;
            }
        });

        setFragment(searchFragment);
    }

    public void setFragment(Fragment fragment) {


        android.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrame, fragment);
        fragmentTransaction.commit();

    }

    private void setUpDataBase() {
        ctx = this.getBaseContext();
        try {
            CopyDataBaseFromAsset();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void CopyDataBaseFromAsset() throws IOException {

        InputStream in = ctx.getAssets().open(DATABASE_NAME);
        Log.w( LOG_TAG , "Starting copying...");
        String outputFileName = DATABASE_PATH + DATABASE_NAME;
        File databaseFolder = new File( DATABASE_PATH2 );

        if ( !databaseFolder.exists() ) {
            databaseFolder.mkdir();
            OutputStream out = new
                    FileOutputStream(outputFileName, false);
            byte[] buffer = new byte[1024];
            int length;
            while ( (length = in.read(buffer)) > 0 ) {
                out.write(buffer,0,length);
            }
            out.flush();
            out.close();
            in.close();
            Log.w(LOG_TAG, "Completed.");
        }
    }

    public void InitDataBase()
    {

        openDatabase = new OpenDatabase(this);
        database = openDatabase.getWritableDatabase();
    }
}
