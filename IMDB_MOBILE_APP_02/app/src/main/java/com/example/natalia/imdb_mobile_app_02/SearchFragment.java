package com.example.natalia.imdb_mobile_app_02;


import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {

    ImageView imgAbout;
    ImageView imgHelp;

    TextView txtSearchEverything;
    Button btnSearchEverything;

    ListView listEverything;
    ArrayList<String> list;
    ArrayAdapter adapter;



    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        initComponents(view);

        return view;
    }

    private void initComponents(View view) {

        imgHelp = (ImageView) view.findViewById(R.id.imgHelp);
        imgHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HelpActivity.class);
                startActivity(intent);
            }
        });

        imgAbout = (ImageView) view.findViewById(R.id.imgAbout);
        imgAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AboutActivity.class);
                startActivity(intent);
            }
        });

        txtSearchEverything = (TextView) view.findViewById(R.id.txtSearchEverything);
        btnSearchEverything = (Button) view.findViewById(R.id.btnSearchEverything);
        listEverything = (ListView) view.findViewById(R.id.listEverything);
        list = new ArrayList<>();
        list = MainActivity.openDatabase.searchEverything(null);
        adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, list);
        listEverything.setAdapter(adapter);
        btnSearchEverything.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = txtSearchEverything.getText().toString();
                list = MainActivity.openDatabase.searchEverything(s);
                adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, list);
                listEverything.setAdapter(adapter);
            }
        });

        listEverything.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String s = (String) parent.getItemAtPosition(position);

                if ( s.contains(",")) {
                    //its a movie
                    String title = s.split(",")[0];
                    String year = s.split(",")[1];
                    Bundle bundle = new Bundle();
                    bundle.putString("title", title);
                    bundle.putString("year", year);
                    SingleMovieFragment fragment = new SingleMovieFragment();
                    fragment.setArguments(bundle);
                    setFragment(fragment);
                } else {
                    //its an actor
                    String firstName = s.split(" ")[0];
                    String lastName = s.split(" ")[1];
                    Bundle bundle = new Bundle();
                    bundle.putString("firstName", firstName);
                    bundle.putString("lastName", lastName);
                    SingleActorFragment fragment = new SingleActorFragment();
                    fragment.setArguments(bundle);
                    setFragment(fragment);
                }
            }
        });
    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrame, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

}
