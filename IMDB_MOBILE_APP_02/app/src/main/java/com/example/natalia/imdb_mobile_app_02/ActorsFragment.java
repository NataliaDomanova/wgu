package com.example.natalia.imdb_mobile_app_02;


import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ActorsFragment extends Fragment {


    TextView txtSearchActor;
    Button btnSearchActor;
    Button  btnAddActor;

    ListView listActors;
    ArrayList<String> actorsList;
    ArrayAdapter adapter;


    public ActorsFragment() {
        // Required empty public constructor
    }

    public void initComponents(View view) {
        txtSearchActor = (TextView) view.findViewById(R.id.txtSearchActor);

        listActors = (ListView) view.findViewById(R.id.listActors);

        btnAddActor = (Button) view.findViewById(R.id.btnAddActor);
        btnAddActor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddActorFragment fragment = new AddActorFragment();
                setFragment(fragment);
            }
        });

        btnSearchActor = (Button) view.findViewById(R.id.btnSearchActor);
        btnSearchActor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshListActors(txtSearchActor.getText().toString());

            }
        });

        listActors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position,
                                    long id)
            {
                //open new activity with it's info
                String fullName = (String) parent.getItemAtPosition(position);
                String firstName = fullName.split(" ")[0];
                String lastName = fullName.split(" ")[1];
                Bundle bundle = new Bundle();
                bundle.putString("firstName", firstName);
                bundle.putString("lastName", lastName);
                SingleActorFragment fragment = new SingleActorFragment();
                fragment.setArguments(bundle);
                setFragment(fragment);
            }
        });
    }

    public void refreshListActors(String line) {
        actorsList = MainActivity.openDatabase.searchActor(line);
        adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, actorsList);
        listActors.setAdapter(adapter);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_actors, container, false);

          initComponents(view);

        refreshListActors(null);
        return view;


    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrame, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }



}
