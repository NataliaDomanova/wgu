package com.example.natalia.imdb_mobile_app_02;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;


public class AddMovieFragment extends Fragment {

    TextView txtTitle;
    TextView txtYear;
    TextView txtBudget;
    TextView txtProfit;

    Spinner spinnerGenres;
    ArrayList<String> genres;
    ArrayAdapter adapterGenres;

    Spinner spinnerActors;
    ArrayList<String> actors;
    ArrayAdapter adapterActors;

    Button btnAddActorToMovie;
    Button btnAddMovie1;

    ListView listActorsInMovie;
    ArrayList<String> actorsInMoviesList;
    ArrayAdapter adapterActorsList;

    public AddMovieFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_movie, container, false);

        initComponents(view);

        return view;
    }

    private void initComponents(View view) {
        txtTitle = (TextView) view.findViewById(R.id.txtTitle);
        txtYear = (TextView) view.findViewById(R.id.txtYear);
        txtBudget = (TextView) view.findViewById(R.id.txtBudget);
        txtProfit = (TextView) view.findViewById(R.id.txtProfit);


        spinnerGenres = (Spinner) view.findViewById(R.id.spinnerGenres);
        genres = new ArrayList<>();
        genres.add("Select a genre");
        genres.addAll(MainActivity.openDatabase.getGenres());
        adapterGenres= new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, genres);
        spinnerGenres.setAdapter(adapterGenres);


        spinnerActors = (Spinner) view.findViewById(R.id.spinnerActors);
        actors = new ArrayList<>();
        actors.add("Select an actor");
        actors.addAll(MainActivity.openDatabase.searchActor(null));
        adapterActors = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, actors);
        spinnerActors.setAdapter(adapterActors);


        listActorsInMovie = (ListView) view.findViewById(R.id.listActorsInMovie);
        actorsInMoviesList = new ArrayList<>();
        adapterActorsList = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, actorsInMoviesList);
        listActorsInMovie.setAdapter(adapterActorsList);


        btnAddActorToMovie = (Button) view.findViewById(R.id.btnAddActorToMovie);
        btnAddActorToMovie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String item = spinnerActors.getSelectedItem().toString();
                if (!actorsInMoviesList.contains(item)) {
                    actorsInMoviesList.add(item);
                    adapterActorsList = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, actorsInMoviesList);
                    listActorsInMovie.setAdapter(adapterActorsList);
                }
            }
        });


        btnAddMovie1 = (Button) view.findViewById(R.id.btnAddMovie1);
        btnAddMovie1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!txtTitle.getText().toString().isEmpty() && !txtYear.getText().toString().isEmpty() &&
                        spinnerGenres.getSelectedItemPosition()!= 0) {
                    String title = txtTitle.getText().toString();
                    int year = Integer.parseInt(txtYear.getText().toString());
                    Integer budget = null;
                    if (!txtBudget.getText().toString().isEmpty()) budget = Integer.parseInt(txtBudget.getText().toString());
                    Integer profit = null;
                    if (!txtProfit.getText().toString().isEmpty()) profit = Integer.parseInt(txtProfit.getText().toString());
                    MainActivity.openDatabase.addMovie(title, year, budget, profit, spinnerGenres.getSelectedItem().toString(),
                            actorsInMoviesList);

                    txtYear.setText("");
                    txtTitle.setText("");
                    txtBudget.setText("");
                    txtProfit.setText("");
                    spinnerGenres.setAdapter(adapterActors);
                    spinnerActors.setAdapter(adapterActors);
                    actorsInMoviesList = new ArrayList<>();
                    adapterActorsList = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, actorsInMoviesList);
                    listActorsInMovie.setAdapter(adapterActorsList);
                }
            }
        });
    }
}
