package com.example.natalia.imdb_mobile_app_02;


import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Date;
import java.util.GregorianCalendar;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddActorFragment extends Fragment {

    TextView txtFirstName;
    TextView txtLastName;
    TextView txtDayB;
    TextView txtMonthB;
    TextView txtYearB;
    TextView txtDayD;
    TextView txtMonthD;
    TextView txtYearD;

    Button btnAddActor1;


    public AddActorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_actor, container, false);

        initComponents(view);

        return view;
    }

    private void initComponents(View view) {
        txtFirstName = (TextView) view.findViewById(R.id.txtFirstName);
        txtLastName = (TextView) view.findViewById(R.id.txtLastName);
        txtDayB = (TextView) view.findViewById(R.id.txtDayB);
        txtMonthB = (TextView) view.findViewById(R.id.txtMonthB);
        txtYearB = (TextView) view.findViewById(R.id.txtYearB);
        txtDayD = (TextView) view.findViewById(R.id.txtDayD);
        txtMonthD = (TextView) view.findViewById(R.id.txtMonthD);
        txtYearD = (TextView) view.findViewById(R.id.txtYearD);

        btnAddActor1 = (Button) view.findViewById(R.id.btnAddActor1);
        btnAddActor1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkInfo()) {
                    String dateBirth = txtYearB.getText().toString()+"-"+txtMonthB.getText().toString()+
                            "-"+txtDayB.getText().toString();

                    if (!txtDayD.getText().toString().isEmpty()||!txtMonthD.getText().toString().isEmpty()
                            ||!txtYearD.getText().toString().isEmpty()) {
                        int dayD = Integer.parseInt(txtDayD.getText().toString());
                        int monthD = Integer.parseInt(txtMonthD.getText().toString());
                        int yearD = Integer.parseInt(txtYearD.getText().toString());
                        Date dateDeath = new GregorianCalendar(yearD, monthD, dayD).getTime();
                        Date date = new Date();
                        if (dateDeath.compareTo(date) >= 0) {

                            String dateDeat = txtYearD.getText().toString()+"-"+txtMonthD.getText().toString()+
                                    "-"+txtDayD.getText().toString();

                            MainActivity.openDatabase.addActor(txtFirstName.getText().toString(),
                                    txtLastName.getText().toString(), dateBirth, dateDeat);
                        }
                    }

                    MainActivity.openDatabase.addActor(txtFirstName.getText().toString(),
                            txtLastName.getText().toString(), dateBirth, null);


                    txtFirstName.setText("");
                    txtLastName.setText("");
                    txtDayB.setText("");
                    txtDayD.setText("");
                    txtMonthB.setText("");
                    txtMonthD.setText("");
                    txtYearB.setText("");
                    txtYearD.setText("");
                }
            }
        });
    }

    private boolean checkInfo() {
        if (!txtFirstName.getText().toString().isEmpty() || !txtLastName.getText().toString().isEmpty()) {

            if (!txtDayB.getText().toString().isEmpty()||!txtMonthB.getText().toString().isEmpty()
                    ||!txtYearB.getText().toString().isEmpty()) {
                int day = Integer.parseInt(txtDayB.getText().toString());
                int month = Integer.parseInt(txtMonthB.getText().toString());
                int year = Integer.parseInt(txtYearB.getText().toString());
                Date dateBirth = new GregorianCalendar(year, month, day).getTime();
                Date date = new Date();
                if (dateBirth.compareTo(date) > 0) return false; //add toast

                return true;
            }

        }
        return false;
    }


}
